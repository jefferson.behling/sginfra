<?php
namespace Reports\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Reports\Model\Table\OccurrencesTable;

/**
 * Reports\Model\Table\OccurrencesTable Test Case
 */
class OccurrencesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Reports\Model\Table\OccurrencesTable
     */
    public $Occurrences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.reports.occurrences',
        'plugin.reports.users',
        'plugin.reports.sectors',
        'plugin.reports.status',
        'plugin.reports.categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Occurrences') ? [] : ['className' => OccurrencesTable::class];
        $this->Occurrences = TableRegistry::get('Occurrences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Occurrences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
