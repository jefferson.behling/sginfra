<style>
    .dropdown-content.select-dropdown.multiple-select-dropdown li:first-child{
        display: none !important;
    }
</style>

<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('reports', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('reports', 'Dashboard'), ['plugin' => false, 'controller' => 'Pages', 'action' => 'home']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('reports', 'Home'), ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'index']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('reports', 'List Sectors'), ['plugin' => 'Infrastructure', 'controller' => 'Sectors', 'action' => 'index']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('reports', 'List Status'), ['plugin' => 'Infrastructure', 'controller' => 'Status', 'action' => 'index']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('reports', 'List Categories'), ['plugin' => 'Infrastructure', 'controller' => 'Categories', 'action' => 'index']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">dashboard</i>',
                        ['plugin' => false, 'controller' => 'Pages', 'action' => 'home'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('reports', 'Dashboard'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">view_agenda</i>',
                        ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('reports', 'Home'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Sectors', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('reports', 'List Sectors'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Status', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('reports', 'List Status'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Categories', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('reports', 'List Categories'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= __d('reports', 'Occurrences - Reports') ?></span>
                <?= $this->Form->create($occurrence) ?>
                <div class="row">
                    <div class="input-field col s12 m6">
                        <?= $this->Form->control('from', ['label' => __d('reports', 'From'), 'class' => 'datepicker']) ?>
                    </div>
                    <div class="input-field col s12 m6">
                        <?= $this->Form->control('to', ['label' => __d('reports', 'To'), 'class' => 'datepicker']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6">
                        <div>
                            <label><?= __d('reports', 'Sectors') ?></label>
                            <select multiple name="sector_id[]">
                                <option value="" disabled selected></option>
                                <?php foreach ($sectors as $id => $sector): ?>
                                    <?php if (in_array($id, $sectors_query)) { ?>
                                        <option value="<?= $id ?>" selected><?= $sector ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $id ?>"><?= $sector ?></option>
                                    <?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col s12 m6">
                        <div>
                            <label><?= __d('reports', 'Categories') ?></label>
                            <select multiple name="category_id[]">
                                <option value="" disabled selected></option>
                                <?php foreach ($categories as $id => $category): ?>
                                    <?php if (in_array($id, $categories_query)) { ?>
                                        <option value="<?= $id ?>" selected><?= $category ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $id ?>"><?= $category ?></option>
                                    <?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col s12 m6">
                        <div>
                            <label><?= __d('reports', 'Status') ?></label>
                            <select multiple name="status_id[]">
                                <option value="" disabled selected></option>
                                <?php foreach ($status as $id => $value): ?>
                                    <?php if (in_array($id, $status_query)) { ?>
                                        <option value="<?= $id ?>" selected><?= $value ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $id ?>"><?= $value ?></option>
                                    <?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?= $this->Form->button(__d('reports', 'Filter'), ['class' => 'btn waves-effect green waves-light col s12 m4 l2']) ?>
                <?= $this->Form->end() ?>
                <div class="table-responsive-vertical shadow-z-1">
                    <table id="table" class="striped table table-hover table-mc-light-blue">
                        <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('title', ['label' => __d('reports', 'Title')]) ?></th>
                                <th><?= $this->Paginator->sort('locale', ['label' => __d('reports', 'Locale')]) ?></th>
                                <th><?= $this->Paginator->sort('sector_id', ['label' => __d('reports', 'Sector')]) ?></th>
                                <th><?= $this->Paginator->sort('status_id', ['label' => __d('reports', 'Status')]) ?></th>
                                <th><?= $this->Paginator->sort('category_id', ['label' => __d('reports', 'Category')]) ?></th>
                                <th><?= $this->Paginator->sort('created', ['label' => __d('reports', 'Created')]) ?></th>
                                <th><?= __d('reports', 'Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($occurrences as $occurrence): ?>
                            <tr>
                                <td data-title="<?= __d('reports', 'Title') ?>"><?= $occurrence->title != null ? h($occurrence->title) : ' - ' ?></td>
                                <td data-title="<?= __d('reports', 'Locale') ?>"><?= $occurrence->locale != null ? h($occurrence->locale) : ' - ' ?></td>
                                <td data-title="<?= __d('reports', 'Sector') ?>"><?= $occurrence->has('sector') ? $this->Html->link($occurrence->sector->name, ['plugin' => 'Infrastructure', 'controller' => 'Sectors', 'action' => 'view', $occurrence->sector->id]) : '' ?></td>
                                <td data-title="<?= __d('reports', 'Status') ?>"><?= $occurrence->has('status') ? $this->Html->link($occurrence->status->name, ['plugin' => 'Infrastructure', 'controller' => 'Status', 'action' => 'view', $occurrence->status->id]) : '' ?></td>
                                <td data-title="<?= __d('reports', 'Category') ?>"><?= $occurrence->has('category') ? $this->Html->link($occurrence->category->name, ['plugin' => 'Infrastructure', 'controller' => 'Categories', 'action' => 'view', $occurrence->category->id]) : '' ?></td>
                                <td data-title="<?= __d('reports', 'Created') ?>"><?= $occurrence->created != null ? h($occurrence->created->format('d/m/Y H:i')) : ' - ' ?></td>
                                <td data-title="<?= __d('reports', 'Actions') ?>">
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('reports', 'View') . '" >zoom_in</i>', ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'view', $occurrence->id], ['escape' => false]) ?>
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('reports', 'Print') . '" >print</i>', ['controller' => 'Pdf', 'action' => 'alone', $occurrence->id], ['target' => '_blank', 'escape' => false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <br><br>
                    <?= $this->Form->create(null, [
                        'type' => 'patch'
                    ]) ?>
                    <?= $this->Form->button('<i class="material-icons left">print</i>'. __d('reports', 'Print'),
                    [
                        'class' => 'waves-effect waves-light green btn',
                        'escape' => false
                    ]) ?>
                    <?= $this->Form->end() ?>
                    <br><br>
                </div>
            </div>
        </div>
        <div class="center">
            <ul class="pagination">
                <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
            </ul>
        </div>
        <p class="right"><?= $this->Paginator->counter(['format' => __d('reports', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
