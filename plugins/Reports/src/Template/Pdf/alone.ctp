<?= $this->layout = false; ?>
<?php $this->response->type('pdf'); ?>
<?= $this->Dompdf->css('materialize'); ?>
<?= $this->Dompdf->css('custom'); ?>
<h5 class="center"><?= __('SGInfra - Sistema de Gerenciamento da Infraestrutura') ?></h5>
<table class="striped">
    <thead>
        <tr>
            <th><?= __d('reports', 'User') ?></th>
            <th><?= __d('reports', 'Title') ?></th>
            <th><?= __d('reports', 'Locale') ?></th>
            <th><?= __d('reports', 'Sector') ?></th>
            <th><?= __d('reports', 'Status') ?></th>
            <th><?= __d('reports', 'Category') ?></th>
            <th><?= __d('reports', 'Created') ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $occurrence->user->name ?></td>
            <td><?= $occurrence->title ?></td>
            <td><?= $occurrence->locale ?></td>
            <td><?= $occurrence->sector->name ?></td>
            <td><?= $occurrence->status->name ?></td>
            <td><?= $occurrence->category->name ?></td>
            <td><?= $occurrence->created->format('d/m/Y H:i:s') ?></td>
        </tr>
    </tbody>
</table>
<br><br>
<footer>
    <h6><?= __d('reports', '<b>Issuer:</b> {0}', $userAuth->name) ?></h6>
    <h6><?= __d('reports', '<b>Date of issue:</b> {0}', date('d/m/Y H:i:s')) ?></h6>
</footer>