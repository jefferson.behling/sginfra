<?php
namespace Reports\Model\Table;

use Cake\Chronos\Chronos;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Occurrences Model
 *
 * @property \Reports\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \Reports\Model\Table\SectorsTable|\Cake\ORM\Association\BelongsTo $Sectors
 * @property \Reports\Model\Table\StatusTable|\Cake\ORM\Association\BelongsTo $Status
 * @property \Reports\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 *
 * @method \Reports\Model\Entity\Occurrence get($primaryKey, $options = [])
 * @method \Reports\Model\Entity\Occurrence newEntity($data = null, array $options = [])
 * @method \Reports\Model\Entity\Occurrence[] newEntities(array $data, array $options = [])
 * @method \Reports\Model\Entity\Occurrence|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Reports\Model\Entity\Occurrence patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Reports\Model\Entity\Occurrence[] patchEntities($entities, array $data, array $options = [])
 * @method \Reports\Model\Entity\Occurrence findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OccurrencesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('occurrences');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'Reports.Users'
        ]);
        $this->belongsTo('Sectors', [
            'foreignKey' => 'sector_id',
            'joinType' => 'INNER',
            'className' => 'Reports.Sectors'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER',
            'className' => 'Reports.Status'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
            'className' => 'Reports.Categories'
        ]);

        $this->searchManager()
            ->add('from', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $values = explode(' ', trim($args['from']));
                    foreach ($values as $value) {
                        $value = str_replace('/', '-', $value);
                        $value = date('Y-m-d', strtotime($value));
                        $query->where(["Occurrences.created >=" => "$value"]);
                    }
                }
            ])
            ->add('to', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $values = explode(' ', trim($args['to']));
                    foreach ($values as $value) {
                        $value = str_replace('/', '-', $value);
                        $value = date('Y-m-d', strtotime($value));
                        $date = Chronos::createFromFormat('Y-m-d', $value);
                        $to = $date->addDay();
                        $query->where(["Occurrences.created <=" => "$to"]);
                    }
                }
            ])
            ->add('sector_id', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $query->where(["Occurrences.sector_id IN" => $args['sector_id']]);
                }
            ])
            ->add('category_id', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $query->where(["Occurrences.category_id IN" => $args['category_id']]);
                }
            ])
            ->add('status_id', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $query->where(["Occurrences.status_id IN" => $args['status_id']]);
                }
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 255)
            ->allowEmpty('locale');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        $validator
            ->scalar('observations')
            ->allowEmpty('observations');

        return $validator;
    }
}
