<?php
namespace Reports\Controller;

use Cake\Chronos\Chronos;
use Cake\Core\Configure;

/**
 * Alone Controller
 */
class PdfController extends Reports
{
    public $helpers = ['Dompdf.Dompdf'];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Reports.PrintReport');
        $this->loadModel('Reports.Occurrences');
    }

    /**
     * Alone method
     *
     * Print only a occurrence
     *
     * @return \Cake\Http\Response|void
     */
    public function alone($id)
    {
        $occurrence = $this->Occurrences->get($id, [
            'contain' => ['Sectors', 'Status', 'Categories', 'Users']
        ]);

        $this->set(compact('occurrence'));
        $this->set('userAuth', $this->Auth->user());

        $this->setTemplate('alone');
        $this->PrintReport->printOut($id);
    }

    /**
     * Alone method
     *
     * Print occurrences
     *
     * @return \Cake\Http\Response|void
     */
    public function occurrences()
    {
        $session = $this->request->getSession();
        $params = $session->read('ReportsParams');

        $occurrences = $this->Occurrences->find('all', [
            'contain' => ['Sectors', 'Status', 'Categories', 'Users']
        ]);

        if (isset($params)) {
            if (isset($params['sector_id'])) {
                $occurrences->where(['sector_id IN' => $params['sector_id']]);
            }

            if (isset($params['category_id'])) {
                $occurrences->where(['category_id IN' => $params['category_id']]);
            }

            if (isset($params['status_id'])) {
                $occurrences->where(['status_id IN' => $params['status_id']]);
            }

            if (isset($params['from'])) {
                $from = str_replace('/', '-', $params['from']);
                $from = date('Y-m-d', strtotime($from));
                $occurrences->where(["Occurrences.created >=" => "$from"]);
            }

            if (isset($params['to'])) {
                $value = str_replace('/', '-', $params['to']);
                $value = date('Y-m-d', strtotime($value));
                $date = Chronos::createFromFormat('Y-m-d', $value);
                $to = $date->addDay();
                $occurrences->where(["Occurrences.created <=" => "$to"]);
            }
        }

        $occurrences->orderDesc('Occurrences.created');

        $this->set(compact('occurrences'));
        $this->set('userAuth', $this->Auth->user());

        $this->setTemplate('occurrences');
        $this->PrintReport->printOut();
    }
}
