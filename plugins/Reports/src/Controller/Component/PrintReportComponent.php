<?php
namespace Reports\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * PrintReport component
 */
class PrintReportComponent extends Component
{

    /**
     * Transforms html into pdf
     *
     * @param string $id
     */
    public function printOut($id = null)
    {
        $viewBuilder = $this->getController()->getViewBuilder();
        $template = $this->getController()->getTemplate();

        $viewBuilder
            ->setTemplate($template)
            ->setClassName('Dompdf.Pdf')
            ->setOptions(['config' => [
                'filename' => $id,
                'render' => 'browser',
                'paginate' => [
                    'x' => 450,
                    'y' => 790,
                    'font' => 'Sans-Serif',
                    'size' => '10px',
                    'text' => false
                ]
            ]
            ]);
    }

}
