<?php
/**
 * Created by PhpStorm.
 * User: jefferson
 * Date: 30/09/18
 * Time: 19:56
 */

namespace Reports\Controller;

class Reports extends AppController
{
    public $template = '';

    /**
     * Get the template that will be used for printing
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set the template that will be used for printing
     *
     * @param $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Create view builder for printing
     *
     * @return \Cake\View\ViewBuilder
     */
    public function getViewBuilder()
    {
        return $this->viewBuilder();
    }
}