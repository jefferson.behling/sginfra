<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'Reports',
    ['path' => '/reports'],
    function (RouteBuilder $routes) {
        $routes->setExtensions(['pdf']);

        $routes->fallbacks(DashedRoute::class);
    }
);
