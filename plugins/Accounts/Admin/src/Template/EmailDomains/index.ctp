<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/admin', 'New Email Domain'), ['action' => 'add']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating cyan tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'New Email Domain'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Email Domains') ?></span>
                <div class="table-responsive-vertical shadow-z-1">
                    <table id="table" class="striped table table-hover table-mc-light-blue">
                        <thead>
                            <tr>
                                <th ><?= $this->Paginator->sort('domain', ['label' => __d('Accounts/admin', 'Domain')]) ?></th>
                                <th ><?= $this->Paginator->sort('active', ['label' => __d('Accounts/admin', 'Active')]) ?></th>
                                <th><?= __d('Accounts/admin', 'Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($emailDomains as $emailDomain): ?>
                            <tr>
                                <td data-title="<?= __d('Accounts/admin', 'Domain') ?>"><?= $emailDomain->domain != null ? h($emailDomain->domain) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/admin', 'Active') ?>"><?= $emailDomain->active ? '<i class="material-icons tiny-custom">check</i>' : '<i class="material-icons tiny-custom">block</i>' ?></td>
                                <td data-title="<?= __d('Accounts/admin', 'Actions') ?>">
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('Accounts/admin', 'Edit') . '" >create</i>', ['action' => 'edit', $emailDomain->id], ['escape' => false]) ?>
                                    <?= $this->Form->postLink('<i class="material-icons tiny-custom black-text" title="'. __d('Accounts/admin', 'Delete') . '" >delete</i>', ['action' => 'delete', $emailDomain->id], ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete # {0}?', $emailDomain->id), 'escape' => false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="center">
            <ul class="pagination">
                <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
            </ul>
        </div>
        <p class="right"><?= $this->Paginator->counter(['format' => __d('Accounts/admin', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
