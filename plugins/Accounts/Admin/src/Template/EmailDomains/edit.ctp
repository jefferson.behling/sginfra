<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Form->postLink(__d('Accounts/admin', 'Delete'),
                            ['action' => 'delete', $emailDomain->id],
                            ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete {0}?', $emailDomain->domain)]
                        ) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Email Domains'), ['action' => 'index']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Form->postLink('<i class="material-icons">delete</i>',
                        ['action' => 'delete', $emailDomain->id],
                        ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete {0}?', $emailDomain->domain),
                        'class' => 'btn-floating red tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'Delete'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Email Domains'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <?= $this->Form->create($emailDomain) ?>
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Edit Email Domain') ?></span>

                <div class="input-field">
                    <?= $this->Form->control('domain', ['label' => __d('Accounts/admin', 'Domain')]) ?>
                </div>
                <p>
                    <?= $this->Form->control('active', ['label' => __d('Accounts/admin', 'Active'), 'type' => 'checkbox', 'class' => 'filled-in', 'hiddenField' => true]) ?>
                </p><br>
                <div class="row">
                    <?= $this->Form->button(__d('Accounts/permissions', 'Submit'), ['class' => 'btn waves-effect green waves-light col s12 m4 l2 offset-l10']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
