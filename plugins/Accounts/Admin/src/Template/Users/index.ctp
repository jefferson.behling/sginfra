<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/admin', 'New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/admin', 'List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Roles', 'action' => 'add'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'New Role'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Roles', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Roles'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Users') ?></span>
                <div class="row">
                    <?= $this->form->create() ?>
                    <div class="input-field col s12">
                        <?= $this->Form->control('name', ['label' => __d('Accounts/admin', 'Filter by name or e-mail')]) ?>
                    </div>
                    <?= $this->Form->button(__d('Accounts/admin', 'Submit'), ['class' => 'btn waves-effect green waves-light col s12 m4 l2']) ?>
                    <?= $this->form->end() ?>
                </div>
                <div class="table-responsive-vertical shadow-z-1">
                    <table id="table" class="striped table table-hover table-mc-light-blue">
                        <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('name', ['label' => __d('Accounts/admin', 'Name')]) ?></th>
                                <th><?= $this->Paginator->sort('email', ['label' => __d('Accounts/admin', 'Email')]) ?></th>
                                <th><?= $this->Paginator->sort('role_id', ['label' => __d('Accounts/admin', 'Role')]) ?></th>
                                <th><?= $this->Paginator->sort('active', ['label' => __d('Accounts/admin', 'Active')]) ?></th>
                                <th><?= __d('Accounts/admin', 'Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td data-title="<?= __d('Accounts/admin', 'Name') ?>"><?= $user->name != null ? h($user->name) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/admin', 'Email') ?>"><?= $user->email != null ? h($user->email) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/admin', 'Role') ?>"><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
                                <td data-title="<?= __d('Accounts/admin', 'Active') ?>"><?= $user->active ? '<i class="material-icons tiny-custom no-hover" title="'. __d('Accounts/admin', 'Active') .'">check</i>' : '<i class="material-icons tiny-custom no-hover" title="'. __d('Accounts/admin', 'Not Active') .'">block</i>' ?></td>
                                <td data-title="<?= __d('Accounts/admin', 'Actions') ?>">
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('Accounts/admin', 'Change role') . '" >create</i>', ['action' => 'edit', $user->id], ['escape' => false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="center">
            <ul class="pagination">
                <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
            </ul>
        </div>
        <p class="right"><?= $this->Paginator->counter(['format' => __d('Accounts/admin', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
