<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'New Role'), ['controller' => 'Roles', 'action' => 'add']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Users'), ['action' => 'index']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Roles', 'action' => 'add'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'New Role'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Roles', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Roles'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Users'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <?= $this->Form->create($user) ?>
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Edit User') ?></span>
                <div class="row">
                    <div class="input-field col s12 m6">
                        <?= $this->Form->control('name', ['label' => __d('Accounts/admin', 'Name')]) ?>
                    </div>
                    <div class="input-field col s12 m6">
                        <?= $this->Form->control('email', ['label' => __d('Accounts/admin', 'E-mail')]) ?>
                    </div>
                    <div class="input-field col s12 m6">
                        <?= $this->Form->control('role_id', ['label' => __d('Accounts/admin', 'Role'), 'options' => $roles]) ?>
                    </div><p class="col s12">
                        <?= $this->Form->control('active', ['label' => __d('Accounts/admin', 'Active'), 'type' => 'checkbox', 'class' => 'filled-in', 'hiddenField' => true]) ?>
                    </p>
                </div>
                <div class="row">
                    <?= $this->Form->button(__d('Accounts/admin', 'Submit'), ['class' => 'btn waves-effect green waves-light col s12 m4 offset-m8 l2 offset-l10']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
