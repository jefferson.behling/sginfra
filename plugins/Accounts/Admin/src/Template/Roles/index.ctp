<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/admin', 'New Role'), ['action' => 'add']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/admin', 'List Permissions'), ['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'index']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/admin', 'List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating cyan tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'New Role'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Permissions'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Users', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Users'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Users', 'action' => 'add'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'New User'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Roles') ?></span>
                <div class="table-responsive-vertical shadow-z-1">
                    <table id="table" class="striped table table-hover table-mc-light-blue">
                        <thead>
                            <tr>
                                <th ><?= $this->Paginator->sort('name', ['label' => __d('Accounts/admin', 'Name')]) ?></th>
                                <th><?= __d('Accounts/admin', 'Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($roles as $role): ?>
                            <tr>
                                <td data-title="<?= __d('Accounts/admin', 'Name') ?>"><?= $role->name != null ? h($role->name) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/admin', 'Actions') ?>">
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('Accounts/admin', 'View') . '" >zoom_in</i>', ['action' => 'view', $role->id], ['escape' => false]) ?>
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('Accounts/admin', 'Edit') . '" >create</i>', ['action' => 'edit', $role->id], ['escape' => false]) ?>
                                    <?= $this->Form->postLink('<i class="material-icons tiny-custom black-text" title="'. __d('Accounts/admin', 'Delete') . '" >delete</i>', ['action' => 'delete', $role->id], ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete # {0}?', $role->id), 'escape' => false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="center">
            <ul class="pagination">
                <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
            </ul>
        </div>
        <p class="right"><?= $this->Paginator->counter(['format' => __d('Accounts/admin', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
