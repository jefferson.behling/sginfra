<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Permissions'), ['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Users'), ['controller' => 'Users', 'action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Roles'), ['action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Form->postLink(__d('Accounts/admin', 'Delete'),
                            ['action' => 'delete', $role->id],
                            ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete # {0}?', $role->id)]
                        ) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Permissions'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Users', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Users'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Roles'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Form->postLink('<i class="material-icons">delete</i>',
                        ['action' => 'delete', $role->id],
                        ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete # {0}?', $role->id),
                        'class' => 'btn-floating red tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'Delete Role'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <?= $this->Form->create($role) ?>
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Edit Role') ?></span>

                <div class="input-field">
                    <?= $this->Form->control('name', ['label' => __d('Accounts/admin', 'Name')]) ?>
                </div>
                <?= $this->Form->button(__d('Accounts/admin', 'Submit'), ['class' => 'btn waves-effect green waves-light']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
