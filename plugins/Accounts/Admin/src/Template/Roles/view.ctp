<div class="row">
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle tooltipped" data-position="left" data-tooltip="<?= __d('Accounts/admin', 'Actions') ?>">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Form->postLink('<i class="material-icons">delete</i>',
                        ['action' => 'delete', $role->id],
                        ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete # {0}?', $role->id),
                            'class' => 'btn-floating red tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'Delete Role'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">mode_edit</i>',
                        ['action' => 'edit', $role->id],
                        ['class' => 'btn-floating green darken-4 tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'Edit Role'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating blue tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'New Role'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Roles'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'index'],
                        ['class' => 'btn-floating orange lighten-1 tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Permissions'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['controller' => 'Users', 'action' => 'index'],
                        ['class' => 'btn-floating orange lighten-1 tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/admin', 'List Users'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/admin', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Form->postLink(__d('Accounts/admin', 'Delete Role'),
                            ['action' => 'delete', $role->id],
                            ['confirm' => __d('Accounts/admin', 'Are you sure you want to delete # {0}?', $role->id)]) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'Edit Role'), ['action' => 'edit', $role->id]) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'New Role'), ['action' => 'add']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Roles'), ['action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Permissions'), ['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/admin', 'List Users'), ['controller' => 'Users', 'action' => 'index']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= h($role->name) ?></span>
                <table class="striped bordered">
                    <tbody>
                        <tr>
                            <td>
                                <?= __d('Accounts/admin', 'Id') ?>
                            </td>
                            <td class="right">
                                <?= h($role->id) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('Accounts/admin', 'Name') ?>
                            </td>
                            <td class="right">
                                <?= h($role->name) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
