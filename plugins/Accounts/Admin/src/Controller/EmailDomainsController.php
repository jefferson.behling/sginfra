<?php
namespace Accounts\Admin\Controller;

use Accounts\Admin\Controller\AppController;
use Cake\Event\Event;

/**
 * EmailDomains Controller
 *
 * @property \Accounts\Admin\Model\Table\EmailDomainsTable $EmailDomains
 *
 * @method \Accounts\Admin\Model\Entity\EmailDomain[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmailDomainsController extends AppController
{

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->viewBuilder()->setHelpers(['Materialize.Form']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $emailDomains = $this->paginate($this->EmailDomains);

        $this->set(compact('emailDomains'));
    }

    /**
     * View method
     *
     * @param string|null $id Email Domain id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $emailDomain = $this->EmailDomains->get($id, [
            'contain' => []
        ]);

        $this->set('emailDomain', $emailDomain);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $emailDomain = $this->EmailDomains->newEntity();
        if ($this->request->is('post')) {
            $emailDomain = $this->EmailDomains->patchEntity($emailDomain, $this->request->getData());
            if ($this->EmailDomains->save($emailDomain)) {
                $this->Flash->success(__d('Accounts/admin', 'The email domain has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__d('Accounts/admin', 'The email domain could not be saved. Please, try again.'));
        }
        $this->set(compact('emailDomain'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Email Domain id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $emailDomain = $this->EmailDomains->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailDomain = $this->EmailDomains->patchEntity($emailDomain, $this->request->getData());
            if ($this->EmailDomains->save($emailDomain)) {
                $this->Flash->success(__d('Accounts/admin', 'The email domain has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__d('Accounts/admin', 'The email domain could not be saved. Please, try again.'));
        }
        $this->set(compact('emailDomain'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Email Domain id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $emailDomain = $this->EmailDomains->get($id);
        if ($this->EmailDomains->delete($emailDomain)) {
            $this->Flash->success(__d('Accounts/admin', 'The email domain has been deleted.'));
        } else {
            $this->Flash->error(__d('Accounts/admin', 'The email domain could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
