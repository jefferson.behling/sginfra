<?php
namespace Accounts\Admin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Roles Model
 *
 * @property \Accounts\Admin\Model\Table\PermissionsTable|\Cake\ORM\Association\HasMany $Permissions
 * @property \Accounts\Admin\Model\Table\UsersTable|\Cake\ORM\Association\HasMany $Users
 *
 * @method \Accounts\Admin\Model\Entity\Role get($primaryKey, $options = [])
 * @method \Accounts\Admin\Model\Entity\Role newEntity($data = null, array $options = [])
 * @method \Accounts\Admin\Model\Entity\Role[] newEntities(array $data, array $options = [])
 * @method \Accounts\Admin\Model\Entity\Role|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Accounts\Admin\Model\Entity\Role patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Accounts\Admin\Model\Entity\Role[] patchEntities($entities, array $data, array $options = [])
 * @method \Accounts\Admin\Model\Entity\Role findOrCreate($search, callable $callback = null, $options = [])
 */
class RolesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('roles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Permissions', [
            'foreignKey' => 'role_id',
            'className' => 'Accounts/Admin.Permissions'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'role_id',
            'className' => 'Accounts/Admin.Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
