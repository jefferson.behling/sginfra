<?php
namespace Accounts\Admin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailDomains Model
 *
 * @method \Accounts\Admin\Model\Entity\EmailDomain get($primaryKey, $options = [])
 * @method \Accounts\Admin\Model\Entity\EmailDomain newEntity($data = null, array $options = [])
 * @method \Accounts\Admin\Model\Entity\EmailDomain[] newEntities(array $data, array $options = [])
 * @method \Accounts\Admin\Model\Entity\EmailDomain|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Accounts\Admin\Model\Entity\EmailDomain patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Accounts\Admin\Model\Entity\EmailDomain[] patchEntities($entities, array $data, array $options = [])
 * @method \Accounts\Admin\Model\Entity\EmailDomain findOrCreate($search, callable $callback = null, $options = [])
 */
class EmailDomainsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('email_domains');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('domain')
            ->maxLength('domain', 255)
            ->requirePresence('domain', 'create')
            ->notEmpty('domain');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }
}
