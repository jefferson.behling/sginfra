<?php
namespace Accounts\Admin\Test\TestCase\Model\Table;

use Accounts\Admin\Model\Table\EmailDomainsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Accounts\Admin\Model\Table\EmailDomainsTable Test Case
 */
class EmailDomainsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Accounts\Admin\Model\Table\EmailDomainsTable
     */
    public $EmailDomains;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.accounts/admin.email_domains'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EmailDomains') ? [] : ['className' => EmailDomainsTable::class];
        $this->EmailDomains = TableRegistry::get('EmailDomains', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmailDomains);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
