<?php
use Migrations\AbstractMigration;

class CreateEmailDomains extends AbstractMigration
{
    public $autoId = false;

    public function change()
    {
        $table = $this->table('email_domains');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('domain', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('active', 'boolean', [
            'default' => true,
            'null' => false
        ]);

        $table->addPrimaryKey('id');
        $table->create();
    }
}
