<?php
use Migrations\AbstractMigration;

class AddRememberTokenUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('remember_token', 'string', [
            'default' => null,
            'limit' => 64,
            'null' => true
        ]);
        $table->update();
    }
}
