<?php
use Migrations\AbstractMigration;

class CreateRoles extends AbstractMigration
{
    public $autoId = false;

    public function change()
    {
        $table = $this->table('roles');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);

        $table->addPrimaryKey('id');
        $table->create();
    }
}
