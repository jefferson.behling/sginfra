<?php
use Migrations\AbstractMigration;

class AddActiveUsers extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('active', 'boolean', [
            'default' => false,
            'null' => false
        ]);

        $table->update();
    }
}
