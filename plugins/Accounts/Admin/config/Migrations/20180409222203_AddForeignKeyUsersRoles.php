<?php
use Migrations\AbstractMigration;

class AddForeignKeyUsersRoles extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('users')
            ->addForeignKey('role_id', 'roles', 'id')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('users')
            ->dropForeignKey('role_id');
    }

}
