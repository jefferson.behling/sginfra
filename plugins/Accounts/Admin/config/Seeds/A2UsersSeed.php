<?php
use Migrations\AbstractSeed;

/**
 * BUsers seed.
 */
class A2UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 'c803da71-35c4-4095-b495-0c0398ef09bb',
                'name' => 'Jefferson Vantuir',
                'email' => 'jefferson.behling@gmail.com',
                'password' => '$2y$10$adD6bKRRpUQG0oAcPylyp.xccP.v4SiX3BzraSAaeHUl3/K/ADUci',
                'role_id' => '91d065a2-8185-42b0-98cf-5ce9cedbd978',
                'created' => '2018-04-09 23:04:12',
                'modified' => '2018-04-09 23:04:12',
                'token' => null,
                'active' => true,
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
