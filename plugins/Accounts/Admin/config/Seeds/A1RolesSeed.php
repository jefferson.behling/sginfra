<?php
use Migrations\AbstractSeed;

/**
 * ARoles seed.
 */
class A1RolesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '91d065a2-8185-42b0-98cf-5ce9cedbd978',
                'name' => 'admin'
            ],
            [
                'id' => 'abfc210b-33d0-462c-ba1f-dec66b7d01f3',
                'name' => 'user'
            ],
            [
                'id' => 'b3e3c3de-ccd5-4fd9-a639-984b9794a307',
                'name' => 'infra'
            ]
        ];

        $table = $this->table('roles');
        $table->insert($data)->save();
    }
}
