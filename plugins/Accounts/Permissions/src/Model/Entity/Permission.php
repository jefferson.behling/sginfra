<?php
namespace Accounts\Permissions\Model\Entity;

use Cake\ORM\Entity;

/**
 * Permission Entity
 *
 * @property string $id
 * @property string $plugin
 * @property string $prefix
 * @property string $extension
 * @property string $controller
 * @property string $action
 * @property bool $allowed
 * @property string $role_id
 *
 * @property \Accounts\Permissions\Model\Entity\Role $role
 */
class Permission extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'plugin' => true,
        'prefix' => true,
        'extension' => true,
        'controller' => true,
        'action' => true,
        'allowed' => true,
        'role_id' => true,
        'role' => true
    ];
}
