<?php
namespace Accounts\Permissions\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Permissions Model
 *
 * @property \Accounts\Permissions\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \Accounts\Permissions\Model\Entity\Permission get($primaryKey, $options = [])
 * @method \Accounts\Permissions\Model\Entity\Permission newEntity($data = null, array $options = [])
 * @method \Accounts\Permissions\Model\Entity\Permission[] newEntities(array $data, array $options = [])
 * @method \Accounts\Permissions\Model\Entity\Permission|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Accounts\Permissions\Model\Entity\Permission patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Accounts\Permissions\Model\Entity\Permission[] patchEntities($entities, array $data, array $options = [])
 * @method \Accounts\Permissions\Model\Entity\Permission findOrCreate($search, callable $callback = null, $options = [])
 */
class PermissionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('permissions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER',
            'className' => 'Accounts/Permissions.Roles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('plugin')
            ->maxLength('plugin', 255)
            ->allowEmpty('plugin');

        $validator
            ->scalar('prefix')
            ->maxLength('prefix', 255)
            ->allowEmpty('prefix');

        $validator
            ->scalar('extension')
            ->maxLength('extension', 255)
            ->allowEmpty('extension');

        $validator
            ->scalar('controller')
            ->maxLength('controller', 255)
            ->allowEmpty('controller');

        $validator
            ->scalar('action')
            ->maxLength('action', 255)
            ->allowEmpty('action');

        $validator
            ->boolean('allowed')
            ->allowEmpty('allowed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }
}
