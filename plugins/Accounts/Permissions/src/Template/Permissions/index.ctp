<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/permissions', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/permissions', 'New Permission'), ['action' => 'select-role']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/permissions', 'New Role'), ['plugin' => 'Accounts/Admin', 'controller' => 'Roles', 'action' => 'add']) ?></li>
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/permissions','List Roles'), ['plugin' => 'Accounts/Admin', 'controller' => 'Roles', 'action' => 'index']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['plugin' => 'Accounts/Admin', 'controller' => 'Roles', 'action' => 'add'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/permissions', 'New Role'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['plugin' => 'Accounts/Admin', 'controller' => 'Roles', 'action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/permissions', 'List Roles'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'select-role'],
                        ['class' => 'btn-floating cyan tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/permissions', 'New Permission'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= __d('Accounts/permissions', 'Permissions') ?></span>
                <div class="table-responsive-vertical shadow-z-1">
                    <table id="table" class="striped table table-hover table-mc-light-blue">
                        <thead>
                            <tr>
                                <th ><?= $this->Paginator->sort('plugin', ['label' => __d('Accounts/permissions', 'Plugin')]) ?></th>
                                <th ><?= $this->Paginator->sort('prefix', ['label' => __d('Accounts/permissions', 'Prefix')]) ?></th>
                                <th ><?= $this->Paginator->sort('extension', ['label' => __d('Accounts/permissions', 'Extension')]) ?></th>
                                <th ><?= $this->Paginator->sort('controller', ['label' => __d('Accounts/permissions', 'Controller')]) ?></th>
                                <th ><?= $this->Paginator->sort('action', ['label' => __d('Accounts/permissions', 'Action')]) ?></th>
                                <th ><?= $this->Paginator->sort('allowed', ['label' => __d('Accounts/permissions', 'Allowed')]) ?></th>
                                <th ><?= $this->Paginator->sort('role_id', ['label' => __d('Accounts/permissions', 'Role')]) ?></th>
                                <th><?= __d('Accounts/permissions', 'Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($permissions as $permission): ?>
                            <tr>
                                <td data-title="<?= __d('Accounts/permissions', 'Plugin') ?>"><?= $permission->plugin != null ? h($permission->plugin) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/permissions', 'Prefix') ?>"><?= $permission->prefix != null ? h($permission->prefix) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/permissions', 'Extension') ?>"><?= $permission->extension != null ? h($permission->extension) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/permissions', 'Controller') ?>"><?= $permission->controller != null ? h($permission->controller) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/permissions', 'Action') ?>"><?= $permission->action != null ? h($permission->action) : ' - ' ?></td>
                                <td data-title="<?= __d('Accounts/permissions', 'Allowed') ?>"><?= $permission->allowed ? '<i class="material-icons tiny-custom no-hover" title="' . __d('Accounts/permissions', 'Allowed') . '">check</i>' : '<i class="material-icons tiny-custom no-hover" title="' . __d('Accounts/permissions', 'Denied') . '">block</i>' ?></td>
                                <td data-title="<?= __d('Accounts/permissions', 'Role') ?>"><?= $permission->has('role') ? $this->Html->link($permission->role->name, ['controller' => 'Roles', 'action' => 'view', $permission->role->id]) : '' ?></td>
                                <td data-title="<?= __d('Accounts/permissions', 'Actions') ?>">
                                    <?= $this->Form->postLink('<i class="material-icons tiny-custom black-text" title="'. __d('Accounts/permissions', 'Delete') . '" >delete</i>', ['action' => 'delete', $permission->id], ['confirm' => __d('Accounts/permissions', 'Are you sure you want to delete # {0}?', $permission->id), 'escape' => false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="center">
            <ul class="pagination">
                <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
            </ul>
        </div>
        <p class="right"><?= $this->Paginator->counter(['format' => __d('Accounts/permissions', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
