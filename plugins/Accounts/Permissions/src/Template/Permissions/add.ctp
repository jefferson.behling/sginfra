<?php
    use Cake\Routing\Router;
?>
<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/permissions', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Html->link(__d('Accounts/permissions', 'List Permissions'), ['action' => 'index']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/permissions', 'List Permissions'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <?= $this->Form->create($permission) ?>
                <span class="card-title green-text"><?= __d('Accounts/permissions', 'Add Permission') ?></span>
                <div class="row">
                    <div class="input-field col s12 m6 l6">
                        <?= $this->Form->control('plugin', ['label' => __d('Accounts/permissions', 'Plugin'), 'empty' => true]) ?>
                    </div>
                    <div class="input-field col s12 m6">
                        <?= $this->Form->control('controller', ['label' => __d('Accounts/permissions', 'Controller'), 'options' => $controllers]) ?>
                    </div>
                    <div id="div-action">
                        <div class="input-field col s12 m6">
                            <?= $this->Form->control('action', ['label' => __d('Accounts/permissions', 'Action'), 'options' =>[]]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?= $this->Form->button(__d('Accounts/permissions', 'Submit'), ['class' => 'btn waves-effect green waves-light col s12 m4 l2 offset-l10']) ?>
                </div>
                <?= $this->Form->end() ?>
                <div class="row">
                    <div class="col s6 m6 l6 offset-s5 offset-m5 offset-l5">
                        <div class="preloader-wrapper small">
                            <div class="spinner-layer spinner-green-only">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div><div class="gap-patch">
                                    <div class="circle"></div>
                                </div><div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#controller').material_select();

    $('#controller').on('contentChanged', function() {
        $(this).material_select();
    });

    $('#action').material_select();

    $('#action').on('contentChanged', function() {
        $(this).material_select();
    });

    $("#div-action").hide();

    $("#plugin").change(function () {
        $.ajax({
            url: '<?= Router::url(['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'listCtls.json']) ?>' +
                '?plugin=' + encodeURIComponent($('#plugin').val()),
            beforeSend: function () {
                $('#controller').empty();
                $('.preloader-wrapper').addClass('active');
            }
        })
            .done(function (data) {
                $('#controller').append('<option value="*">*</option>');
                $.each(data.controllers, function (i, item) {
                    var $newOpt = $("<option>").attr("value",i).text(item)
                    $("#controller").append($newOpt);
                    $("#controller").trigger('contentChanged');
                })
                $('.preloader-wrapper').removeClass('active');
            })
    });

    $("#controller").change(function () {
        if ($(this).val() == "*") {
            $('#action').val('*');
            $('#div-action').hide();
            return;
        }
        loadControllers();
    });

    loadControllers = function () {
        $.ajax({
            url: '<?= Router::url(['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'listActions.json']) ?>' +
                '?plugin=' + encodeURIComponent($('#plugin').val()) + '&controller='+ encodeURIComponent($('#controller').val()),
            beforeSend: function () {
                $('#action').empty();
                $('#div-action').show();
                $('.preloader-wrapper').addClass('active');
            }
        })
            .done(function (data) {
                $('#action').append('<option value="*">*</option>');
                $.each(data.actions, function (i, item) {
                    var $newOpt = $("<option>").attr("value",i).text(item)
                    $("#action").append($newOpt);
                    $("#action").trigger('contentChanged');
                })
                $('.preloader-wrapper').removeClass('active');
            })
    }

    $(document).ready(function () {
        loadControllers();
    })
</script>