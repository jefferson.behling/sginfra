<?php
namespace Accounts\Permissions\Test\TestCase\Controller\Component;

use Accounts\Permissions\Controller\Component\CheckPermissionsComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * Accounts\Permissions\Controller\Component\CheckPermissionsComponent Test Case
 */
class CheckPermissionsComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Accounts\Permissions\Controller\Component\CheckPermissionsComponent
     */
    public $CheckPermissions;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->CheckPermissions = new CheckPermissionsComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CheckPermissions);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
