<?php
use Migrations\AbstractMigration;

class AddForeignKeyPermissionsRoles extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('permissions')
            ->addForeignKey('role_id', 'roles', 'id')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('permissions')
            ->dropForeignKey('role_id');
    }
}
