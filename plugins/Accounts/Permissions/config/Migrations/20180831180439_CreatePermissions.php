<?php
use Migrations\AbstractMigration;

class CreatePermissions extends AbstractMigration
{
    public $autoId = false;

    public function change()
    {
        $table = $this->table('permissions');
        $table->addColumn('id', 'uuid', [
                'default' => null,
                'limit' => 255,
                'null' => false
            ])
            ->addColumn('plugin', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('prefix', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('extension', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('controller', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('allowed', 'boolean', [
                'default' => true,
                'null' => true
            ])
            ->addColumn('role_id', 'uuid', [
                'default' => null,
                'null' => false
            ]);

        $table->addPrimaryKey('id');
        $table->create();
    }
}
