<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'Accounts/Permissions',
    ['path' => '/accounts/permissions'],
    function (RouteBuilder $routes) {
        $routes->setExtensions(['json']);
        $routes->fallbacks('DashedRoute');
    }
);