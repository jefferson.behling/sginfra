<?php
use Migrations\AbstractSeed;

/**
 * A1Permissions seed.
 */
class A1PermissionsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '8b1677fa-37cc-49b2-b506-2b987fb036ed',
                'role_id' => '91d065a2-8185-42b0-98cf-5ce9cedbd978', // admin
                'plugin' => '*',
                'prefix' => '*',
                'controller' => '*',
                'action' => '*',
                'extension' => '*',
                'allowed' => true,
            ],
            [
                'id' => '168f9255-433a-4e61-98fa-028470ce7eba',
                'role_id' => 'abfc210b-33d0-462c-ba1f-dec66b7d01f3', // user
                'plugin' => 'Infrastructure',
                'prefix' => null,
                'controller' => 'Occurrences',
                'action' => '*',
                'extension' => null,
                'allowed' => true,
            ],
            [
                'id' => '03dfad3e-e7aa-4fd3-b5cd-8f02acde0aa2',
                'role_id' => 'b3e3c3de-ccd5-4fd9-a639-984b9794a307', // infra
                'plugin' => null,
                'prefix' => null,
                'controller' => 'Pages',
                'action' => 'home',
                'extension' => null,
                'allowed' => true,
            ],
            [
                'id' => '805aaa1f-792b-48fd-9a17-0c1f135d0af2',
                'role_id' => 'b3e3c3de-ccd5-4fd9-a639-984b9794a307', // infra
                'plugin' => 'Infrastructure',
                'prefix' => null,
                'controller' => '*',
                'action' => '*',
                'extension' => null,
                'allowed' => true,
            ],
        ];

        $table = $this->table('permissions');
        $table->insert($data)->save();
    }
}
