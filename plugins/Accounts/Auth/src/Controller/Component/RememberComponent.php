<?php
namespace Accounts\Auth\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Remember component
 */
class RememberComponent extends Component
{

    public $components = ['Auth', 'Cookie'];

    public function authRemember($model, $user, $setCookie = true)
    {
        $this->Auth->setUser($user);

        if ($setCookie) {
            $this->Cookie->config([
                'expires' => '+30 days'
            ]);
            $token = bin2hex(random_bytes(32));
            $user->remember_token = $token;

            $cookie = [
                'id' => $user->id,
                'token' => $user->remember_token
            ];

            $model->save($user);
            $this->Cookie->write('remember', $cookie);
        }

        $controller = $this->_registry->getController();

        return $controller->redirect($this->Auth->redirectUrl());
    }

    public function logoutRemember($model)
    {
        $this->Cookie->delete('remember');

        $user = $model->get($this->Auth->user('id'));
        $user->remember_token = null;

        $model->save($user);

        $controller = $this->_registry->getController();

        return $controller->redirect($this->Auth->logout());
    }
}
