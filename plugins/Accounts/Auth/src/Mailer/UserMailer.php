<?php
namespace Accounts\Auth\Mailer;

use Cake\Mailer\Mailer;

/**
 * User mailer.
 */
class UserMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'User';

    public function welcome($user)
    {
        $this->to($user->email)
            ->setProfile('default')
            ->setEmailFormat('html')
            ->setTemplate('Accounts/Auth.welcome')
            ->setLayout('default')
            ->setViewVars(['name' => $user->name, 'token' => $user->token])
            ->setSubject('Bem-vindo, '. $user->name);
    }

    public function recoveryPass($user)
    {
        $this->to($user[0]['email'])
            ->setProfile('default')
            ->setEmailFormat('html')
            ->setTemplate('Accounts/Auth.recovery_pass')
            ->setLayout('default')
            ->setViewVars(['user' => $user[0]])
            ->setSubject('Recuperação de Senha');
    }
}
