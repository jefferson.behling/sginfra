<h2>SGInfra - Sistema de Gerenciamento da Infraestrutura</h2>
<p>
    Olá <?= $user['name'] ?>
</p>
<p>
    Você solicitou alteração de senha. Se não foi você, ignore este e-mail. Caso tenha sido você, basta clicar no link abaixo
    e digitar sua nova senha.
</p>
<?php $url = $this->Url->build(['plugin' => 'Accounts/Auth', 'controller' => 'Users', 'action' => 'recovery-pass'], true); ?>
<?php $url = $url . '?h='. md5(substr($user['password'], 0, 30)) . '&i=' . $user['id']; ?>
<p>
    <?= $this->Html->link('Redefina sua senha clicando no link', $url) ?>
</p>
