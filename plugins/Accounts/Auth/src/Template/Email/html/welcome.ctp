<p>Seja bem-vindo, <?= $name ?></p>
<?php $urlToken = $url = $this->Url->build(['plugin' => 'Accounts/Auth', 'controller' => 'Users', 'action' => 'activate-account', $token], true); ?>
<p>Antes de começar a usar o Sistema, confirme sua conta clicando
    <a href="<?= $urlToken ?>" target="_blank"> aqui</a>.
<p>
    Após ativar sua conta, você será estará livre para acessar sua conta.
</p>
<p>
    <?php $url = $this->Url->build(['controller' => 'Users', 'action' => 'login'], true); ?>
    <a href="<?= $url ?>" target="_blank">Faça login</a>
</p>