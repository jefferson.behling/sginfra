<?php
    use Cake\Core\Configure;
    $this->layout = 'login';
?>
<div class="users form">
    <?= $this->Flash->render('auth') ?>
    <div class="row">
        <div class="col s12">
            <div class="card-panel z-depth-4 darken-1 col s12 m4 offset-m4 flow-text">
                <div class="card-content black-text">
                    <div class="center">
                        <p class="login-form-text"><?= __d('Accounts/auth', 'Password Recovery') ?></p>
                    </div>
                    <?= $this->Form->create() ?>
                    <div class="input-field margin-right-10">
                        <i class="material-icons prefix">contact_mail</i>
                        <?= $this->Form->control('email', ['label' => __d('Accounts/auth', 'Email'), 'class' => 'validate', 'required' => true]) ?>
                    </div>
                    <div class="row">
                        <div class="col s12 margin-bottom-5">
                            <div class="text-size-12">
                                <p>
                                    <?= $this->Html->link(__d('Accounts/auth', 'Sign In'), ['action' => 'login']); ?>
                                </p>
                            </div>
                            <div class="text-size-12">
                                <p>
                                    <?= $this->Html->link(__d('Accounts/auth', 'Register Now'), ['action' => 'register']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="input-field margin-bottom-5">
                        <?= $this->Form->submit(__d('Accounts/auth', 'Submit'), ['class' => 'btn waves-light green col s12 m10 l6 offset-m1 offset-l3']) ?>
                        <br><br>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

