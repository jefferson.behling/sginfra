<?php
    $this->layout = 'login';
?>
<div class="users form">
    <?= $this->Flash->render('auth') ?>
    <div class="row">
        <div class="col s12">
            <div class="card-panel z-depth-4 darken-1 col s12 l4 offset-l4 flow-text">
                <div class="card-content black-text">
                    <div class="center">
                        <p class="login-form-text"><?= __d('Accounts/auth', 'Sign up, it\'s free') ?></p>
                    </div>
                    <?= $this->Form->create($user) ?>
                    <div class="input-field margin-right-10">
                        <i class="material-icons prefix">contacts</i>
                        <?= $this->Form->control('name', ['label' => __d('Accounts/auth', 'Name')]) ?>
                    </div>
                    <div class="input-field margin-right-10">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">contact_mail</i>
                                <?= $this->Form->control('email_prefix', ['label' => __d('Accounts/auth', 'Email'), 'class' => 'prefix']) ?>
                            </div>
                            <div class="input-field col s6">
                                <?= $this->Form->control('domain', ['type' => 'select', 'label' => false, 'options' => $emailDomains]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="input-field margin-right-10">
                        <i class="material-icons prefix">lock</i>
                        <?= $this->Form->control('password', ['label' => __d('Accounts/auth', 'Password')]) ?>
                    </div>
                    <div class="input-field margin-right-10">
                        <i class="material-icons prefix">lock</i>
                        <?= $this->Form->control('confirm_password', ['type' => 'password', 'label' => __d('Accounts/auth', 'Confirm Password')]) ?>
                    </div>
                    <div class="row">
                        <div class="col s12 margin-bottom-5">
                            <div class="text-size-12">
                                <p>
                                    <?= __d('Accounts/auth', 'Already have an account? ') ?>
                                    <?= $this->Html->link(__d('Accounts/auth', 'Sign in'), ['action' => 'login']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="input-field margin-bottom-5">
                        <?= $this->Form->submit(__d('Accounts/auth', 'Sign Up'), ['class' => 'btn waves-light green col s12 m10 l6 offset-m1 offset-l3', 'id' => 'btn-register']) ?>
                        <br><br>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
<script>
    $(document).ready(function() {
        $('select').material_select();
    });

    $("#email-prefix").keyup(function () {
       if ($(this).val().match("@")) {
           $(this).addClass("is-invalid");
       } else {
           $(this).removeClass('is-invalid');
       }
    });

</script>