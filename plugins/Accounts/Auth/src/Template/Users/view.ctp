<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __d('Accounts/auth', 'Actions') ?></li>
        <li><?= $this->Html->link(__d('Accounts/auth', 'Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__d('Accounts/auth', 'Delete User'), ['action' => 'delete', $user->id], ['confirm' => __d('Accounts/auth', 'Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__d('Accounts/auth', 'List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__d('Accounts/auth', 'New User'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __d('Accounts/auth', 'Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __d('Accounts/auth', 'Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __d('Accounts/auth', 'Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __d('Accounts/auth', 'Role') ?></th>
            <td><?= $user->has('role') ? $this->Html->link($user->role->role, ['plugin' => 'Accounts/Admin', 'controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __d('Accounts/auth', 'Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __d('Accounts/auth', 'Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
</div>
