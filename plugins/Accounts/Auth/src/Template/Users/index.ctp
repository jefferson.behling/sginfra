<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $users
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __d('Accounts/auth', 'Actions') ?></li>
        <li><?= $this->Html->link(__d('Accounts/auth', 'New User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__d('Accounts/auth', 'List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__d('Accounts/auth', 'New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __d('Accounts/auth', 'Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th ><?= $this->Paginator->sort('name') ?></th>
                <th ><?= $this->Paginator->sort('username') ?></th>
                <th ><?= $this->Paginator->sort('email') ?></th>
                <th ><?= $this->Paginator->sort('role_id') ?></th>
                <th ><?= $this->Paginator->sort('created') ?></th>
                <th ><?= $this->Paginator->sort('modified') ?></th>
                <th  class="actions"><?= __d('Accounts/auth', 'Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->name) ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= $user->has('role') ? $this->Html->link($user->role->role, ['plugin' => 'Accounts/Admin', 'controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
                <td><?= h($user->created) ?></td>
                <td><?= h($user->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__d('Accounts/auth', 'View'), ['action' => 'view', $user->id]) ?>
                    <?= $this->Html->link(__d('Accounts/auth', 'Edit'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink(__d('Accounts/auth', 'Delete'), ['action' => 'delete', $user->id], ['confirm' => __d('Accounts/auth', 'Are you sure you want to delete # {0}?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __d('Accounts/auth', 'first')) ?>
            <?= $this->Paginator->prev('< ' . __d('Accounts/auth', 'previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__d('Accounts/auth', 'next') . ' >') ?>
            <?= $this->Paginator->last(__d('Accounts/auth', 'last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __d('Accounts/auth', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
