<?php
    use Cake\Core\Configure;
    $this->layout = 'login';
?>
<div class="users form">
    <?= $this->Flash->render('auth') ?>
    <div class="row">
        <div class="col s12">
            <div class="card-panel z-depth-4 darken-1 col s12 m4 offset-m4 flow-text">
                <div class="card-content black-text">
                    <div class="input-field col s12 center">
                        <?= $this->Html->image('logo.png', ['class' => 'responsive-img', 'width' => '150', 'height' => '150']) ?>
                    </div>
                    <div class="center">
                        <div class="login-form-text">
                            <div style="font-weight: 300">
                                <?= __d('Accounts/auth', 'SGInfra') ?>
                            </div>
                            <div style="font-size: 15px">
                                <?= __d('Accounts/auth', 'Sistema de Gerenciamento da Infraestrutura') ?>
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->create() ?>
                    <div class="input-field margin-right-10">
                        <i class="material-icons prefix">contact_mail</i>
                        <?= $this->Form->control('email', ['label' => __d('Accounts/auth', 'E-mail'), 'class' => 'validate']) ?>
                    </div>
                    <div class="input-field margin-right-10">
                        <i class="material-icons prefix">lock</i>
                        <?= $this->Form->control('password', ['label' => __d('Accounts/auth', 'Password')]) ?>
                    </div>
                    <p>
                        <?= $this->Form->control('remember', ['type' => 'checkbox', 'label' => __d('Accounts/auth', 'Keep me connected'), 'class' => 'filled-in']) ?>
                    </p>
                    <div class="row">
                        <div class="col s12 margin-bottom-5 flow-text">
                            <div class="text-size-12">
                                <p>
                                    <?= $this->Html->link(__d('Accounts/auth', 'Register Now'), ['action' => 'register']); ?>
                                </p>
                            </div>
                            <div class="text-size-12">
                                <p>
                                    <?= $this->Html->link(__d('Accounts/auth', 'Forgot password?'), ['action' => 'forgot-password']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="input-field margin-bottom-5">
                        <?= $this->Form->submit(__d('Accounts/auth', 'Login'), ['class' => 'btn waves-ligh green col s12 m10 l6 offset-m1 offset-l3']) ?>
                        <br><br>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
