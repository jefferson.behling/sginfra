<?php
namespace Accounts\Profile\Controller;

use Accounts\Profile\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use CakeDC\Users\Controller\Traits\CustomUsersTableTrait;
use CakeDC\Users\Controller\Traits\PasswordManagementTrait;
use CakeDC\Users\Controller\Traits\ProfileTrait;
use CakeDC\Users\Exception\UserNotFoundException;
use CakeDC\Users\Exception\WrongPasswordException;
use Exception;

/**
 * Users Controller
 *
 * @property \Accounts\Profile\Model\Table\UsersTable $Users
 *
 * @method \Accounts\Profile\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    use CustomUsersTableTrait;
    use ProfileTrait;
    use PasswordManagementTrait;

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->viewBuilder()->setHelpers(['Materialize.Form']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $user = $this->Auth->user();

        $this->set(compact('user'));
    }

    public function changePassword($id = null)
    {
        $id = $this->Auth->user('id');
        $user = $this->Users->newEntity();
        if (!empty($id)) {
            $user->id = $this->Auth->user('id');
            $validatePassword = true;
            //@todo add to the documentation: list of routes used
            $redirect = ['plugin' => 'Accounts/Profile', 'controller' => 'Users', 'action' => 'index'];
        } else {
            $user->id = $this->request->getSession()->read(Configure::read('Users.Key.Session.resetPasswordUserId'));
            $validatePassword = false;
            //@todo add to the documentation: list of routes used
            $redirect = $this->Auth->getConfig('loginAction');
        }

        $this->set('validatePassword', $validatePassword);
        if ($this->request->is('post')) {
            try {
                $user = $this->Users->patchEntity($user, $this->request->getData(), ['validate' => 'passwordConfirm']);

                $currentUser = $this->Users->get($user->id, [
                    'contain' => []
                ]);

                if (!$user->checkPassword($user->current_password, $currentUser->password)) {
                    $this->Flash->error(__d('Accounts/profile', 'The old password does not match'));
                    $this->set(compact('user'));
                    $this->set('_serialize', ['user']);
                    return;
                }

                if ($user->getErrors()) {
                    $this->Flash->error(__d('Accounts/profile', 'Password could not be changed'));
                } else {
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__d('Accounts/profile', 'Password has been changed successfully'));
                        return $this->redirect($redirect);
                    } else {
                        $this->Flash->error(__d('Accounts/profile', 'Password could not be changed'));
                    }
                }
            } catch (UserNotFoundException $exception) {
                $this->Flash->error(__d('Accounts/profile', 'User was not found'));
            } catch (WrongPasswordException $wpe) {
                $this->Flash->error(__d('Accounts/profile', 'The current password does not match'));
            } catch (Exception $exception) {
                $this->Flash->error(__d('Accounts/profile', 'Password could not be changed'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
}
