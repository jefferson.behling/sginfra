<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('Accounts/profile','Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('Accounts/profile','Profile'), ['action' => 'index']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">person</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating orange tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('Accounts/profile','Profile'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m10 offset-m1 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <?= $this->Form->create() ?>
                <span class="card-title green-text"><?= __d('Accounts/profile','Please enter the new password') ?></span>
                <?php if ($validatePassword) : ?>
                    <div class="row">
                        <div class="col s12">
                            <div class="input-field required">
                                <?= $this->Form->control('current_password', [
                                    'type' => 'password',
                                    'required' => true,
                                    'label' => __d('Accounts/profile', 'Current password')]) ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col s12">
                        <div class="input-field required">
                            <?= $this->Form->control('password', ['label' => __d('Accounts/profile','Password')]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="input-field required">
                            <?= $this->Form->control('password_confirm', ['type' => 'password', 'required' => true, 'label' => __d('Accounts/profile', 'Password Confirm')]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m12 l12">
                        <?= $this->Form->button(__d('Accounts/profile','Submit'), ['class' => 'btn waves-effect waves-light col s12 m3 l3 offset-m9 offset-l9 green-background']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
