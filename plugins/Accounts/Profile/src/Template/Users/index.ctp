<div class="row">
    <div class="col s12">
        <div class="card darken-1">
            <div class="card-content black-text">
                <?php
                    $email_hash = md5(strtolower(trim($user->email)));
                ?>
                <div class="row">
                    <div class="col s12 m4 l3">
                        <div class="col s12">
                            <?= $this->Html->image(
                                'https://www.gravatar.com/avatar/' . $email_hash . "?s=400",
                                ['alt' => $user->name, 'title' => $user->name, 'height' => '200', 'width' => '200', 'data-beloworigin' => 'true', 'class' => 'circle materialboxed']
                            ) ?>
                        </div>
                        <div class="col s12 m10 offset-m1 l8 offset-l2">
                            <?= $this->Html->link(__d('Accounts/profile', 'Change Gravatar'), 'https://br.gravatar.com/') ?>
                        </div>
                        <div class="col s12 m10 offset-m1 l8 offset-l2">
                            <?= $this->Html->link(__d('Accounts/profile', 'Change Password'), ['action' => 'change-password']) ?>
                        </div>
                    </div>
                    <div class="col s12 m8 l9">
                        <span class="card-title green-text"><?= h($user->name) ?></span>
                        <table class="striped bordered">
                            <tbody>
                            <tr>
                                <td>
                                    <?= __d('Accounts/profile', 'Name') ?>
                                </td>
                                <td class="right">
                                    <?= h($user->name) ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= __d('Accounts/profile', 'Email') ?>
                                </td>
                                <td class="right">
                                    <?= h($user->email) ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= __d('Accounts/profile', 'Created') ?>
                                </td>
                                <td class="right">
                                    <?= h($user->created->format('d/m/Y H:i')) ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= __d('Accounts/profile', 'Modified') ?>
                                </td>
                                <td class="right">
                                    <?= h($user->modified->format('d/m/Y H:i')) ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.materialboxed').materialbox();
    });
</script>