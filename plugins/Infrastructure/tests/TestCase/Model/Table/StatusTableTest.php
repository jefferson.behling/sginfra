<?php
namespace Infrastructure\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Infrastructure\Model\Table\StatusTable;

/**
 * Infrastructure\Model\Table\StatusTable Test Case
 */
class StatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Infrastructure\Model\Table\StatusTable
     */
    public $Status;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.infrastructure.status',
        'plugin.infrastructure.occurrences'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Status') ? [] : ['className' => StatusTable::class];
        $this->Status = TableRegistry::get('Status', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Status);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
