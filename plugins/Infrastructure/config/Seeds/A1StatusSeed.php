<?php
use Migrations\AbstractSeed;

/**
 * A1Status seed.
 */
class A1StatusSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 'b2f8b324-23d9-435b-b90d-0583b97743ae',
                'name' => 'Enviado'
            ],
            [
                'id' => 'c31beee8-6ad3-46a3-bb6e-4bc7d74d0ee2',
                'name' => 'Recebido'
            ],
            [
                'id' => '4796743a-6567-48bc-a643-b8413c5fcbd9',
                'name' => 'Negado'
            ],
            [
                'id' => '2bcf96f3-f0e8-40ef-9df2-28b16fda7ff5',
                'name' => 'Em Andamento'
            ],
            [
                'id' => 'c058a194-1d02-4fe6-a02f-6684c009e78c',
                'name' => 'Finalizado'
            ],
        ];

        $table = $this->table('status');
        $table->insert($data)->save();
    }
}
