<?php
use Migrations\AbstractMigration;

class AddForeignKeyOccurrencesCategories extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('occurrences')
            ->addForeignKey('category_id', 'categories', 'id')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('occurrences')
            ->dropForeignKey('category_id');
    }
}
