<?php
use Migrations\AbstractMigration;

class AddForeignKeyParentIdSectorsSectors extends AbstractMigration
{
    public function up()
    {
        $this->table('sectors')
            ->addForeignKey('parent_id', 'sectors', 'id')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('sectors')
            ->dropForeignKey('parent_id');
    }
}
