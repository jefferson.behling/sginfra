<?php
use Migrations\AbstractMigration;

class CreateSectors extends AbstractMigration
{
    public $autoId = false;

    public function change()
    {
        $table = $this->table('sectors');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false
        ]);
        $table->addColumn('sector_id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addPrimaryKey('id');
        $table->create();
    }
}
