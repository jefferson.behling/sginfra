<?php
use Migrations\AbstractMigration;

class AddForeignKeyOccurrencesUsers extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('occurrences')
            ->addForeignKey('user_id', 'users', 'id')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('occurrences')
            ->dropForeignKey('user_id');
    }
}
