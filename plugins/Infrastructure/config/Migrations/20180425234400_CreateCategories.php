<?php
use Migrations\AbstractMigration;

class CreateCategories extends AbstractMigration
{
    public $autoId = false;

    public function change()
    {
        $table = $this->table('categories');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false
        ]);
        $table->addPrimaryKey('id');
        $table->create();
    }
}
