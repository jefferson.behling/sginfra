<?php
use Migrations\AbstractMigration;

class AddForeignKeyOccurrencesStatus extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('occurrences')
            ->addForeignKey('status_id', 'status', 'id')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('occurrences')
            ->dropForeignKey('status_id');
    }
}
