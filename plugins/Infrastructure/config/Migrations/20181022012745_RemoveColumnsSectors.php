<?php
use Migrations\AbstractMigration;

class RemoveColumnsSectors extends AbstractMigration
{
    public function change()
    {
        $this->table('sectors')
            ->removeColumn('parent_id')
            ->removeColumn('lft')
            ->removeColumn('rght')
            ->update();
    }
}
