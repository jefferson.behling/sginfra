<?php
use Migrations\AbstractMigration;

class RemoveColumnsCategories extends AbstractMigration
{
    public function change()
    {
        $this->table('categories')
            ->removeColumn('parent_id')
            ->removeColumn('lft')
            ->removeColumn('rght')
            ->update();
    }
}
