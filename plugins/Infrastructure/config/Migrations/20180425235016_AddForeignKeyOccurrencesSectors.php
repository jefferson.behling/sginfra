<?php
use Migrations\AbstractMigration;

class AddForeignKeyOccurrencesSectors extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('occurrences')
            ->addForeignKey('sector_id', 'sectors', 'id')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('occurrences')
            ->dropForeignKey('sector_id');
    }
}
