<?php
use Migrations\AbstractMigration;

class AddCreatedModifiedOccurrences extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('occurrences');
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true
        ]);

        $table->update();
    }
}
