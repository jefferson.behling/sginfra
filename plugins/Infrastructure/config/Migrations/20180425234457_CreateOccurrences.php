<?php
use Migrations\AbstractMigration;

class CreateOccurrences extends AbstractMigration
{
    public $autoId = false;

    public function change()
    {
        $table = $this->table('occurrences');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('title', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('description', 'text', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('locale', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ]);
        $table->addColumn('image', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('observations', 'text', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('user_id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('sector_id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('status_id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('category_id', 'uuid', [
            'default' => null,
            'null' => false
        ]);
        $table->addPrimaryKey('id');
        $table->create();
    }
}
