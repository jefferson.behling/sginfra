<?php
use Migrations\AbstractMigration;

class AlterSectorIdToNullSectors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('sectors');
        $table->changeColumn('sector_id', 'uuid', [
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }
}
