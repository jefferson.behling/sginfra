<?php
namespace Infrastructure\Controller;

use Infrastructure\Controller\AppController;
use Cake\Event\Event;

/**
 * Occurrences Controller
 *
 * @property \Infrastructure\Model\Table\OccurrencesTable $Occurrences
 *
 * @method \Infrastructure\Model\Entity\Occurrence[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OccurrencesController extends AppController
{

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->viewBuilder()->setHelpers(['Materialize.Form']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->request->is('post')) {
            $occurrence = $this->Occurrences->getRecords($this->request->getData());
            $occurrence->user_id = $this->Auth->user('id');

            $this->add($occurrence);
        }
        $this->paginate = [
            'contain' => ['Users', 'Sectors', 'Status', 'Categories'],
            'order' => [
                'Occurrences.created' => 'desc'
            ],
        ];

        $occurrences = $this->paginate($this->Occurrences);

        $sectors = $this->Occurrences->Sectors->find('list')->orderAsc('name');
        $categories = $this->Occurrences->Categories->find('list')->orderAsc('name');
        $this->set(compact('occurrences', 'sectors', 'categories'));
    }

    public function news()
    {
        $this->paginate = [
            'contain' => ['Users', 'Sectors', 'Status', 'Categories'],
            'order' => [
                'Occurrences.created' => 'desc'
            ],
            'conditions' => [
                'Occurrences.status_id IN' => [SENT, RECEIVED]
            ]
        ];

        $occurrences = $this->paginate($this->Occurrences);

        $this->set(compact('occurrences'));
    }

    public function progress()
    {
        $this->paginate = [
            'contain' => ['Users', 'Sectors', 'Status', 'Categories'],
            'order' => [
                'Occurrences.created' => 'desc'
            ],
            'conditions' => [
                'Occurrences.status_id' => IN_PROGRESS
            ]
        ];

        $occurrences = $this->paginate($this->Occurrences);

        $this->set(compact('occurrences'));
    }

    public function finished()
    {
        $this->paginate = [
            'contain' => ['Users', 'Sectors', 'Status', 'Categories'],
            'order' => [
                'Occurrences.created' => 'desc'
            ],
            'conditions' => [
                'Occurrences.status_id' => FINISHED
            ]
        ];

        $occurrences = $this->paginate($this->Occurrences);

        $this->set(compact('occurrences'));
    }

    public function denied()
    {
        $this->paginate = [
            'contain' => ['Users', 'Sectors', 'Status', 'Categories'],
            'order' => [
                'Occurrences.created' => 'desc'
            ],
            'conditions' => [
                'Occurrences.status_id' => DENIED
            ]
        ];

        $occurrences = $this->paginate($this->Occurrences);

        $this->set(compact('occurrences'));
    }

    /**
     * View method
     *
     * @param string|null $id Occurrence id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $occurrence = $this->Occurrences->get($id, [
            'contain' => ['Users', 'Sectors', 'Status', 'Categories']
        ]);

        $this->set('occurrence', $occurrence);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($entity = null)
    {
        if ($this->request->is('post')) {
            $occurrence = $this->Occurrences->getRecords($this->request->getData());
            $occurrence->user_id = $this->Auth->user('id');
            $occurrence->status_id = SENT;

            if ($this->Occurrences->save($occurrence)) {
                $this->Flash->success(__d('infrastructure', 'The occurrence has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__d('infrastructure', 'The occurrence could not be saved. Please, try again.'));

        } else if ($entity != null) {
            if ($this->Occurrences->save($entity)) {
                $this->Flash->success(__d('infrastructure', 'The occurrence has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__d('infrastructure', 'The occurrence could not be saved. Please, try again.'));
            return $this->redirect(['action' => 'index']);
        }
        $sectors = $this->Occurrences->Sectors->find('list')->orderAsc('name');
        $categories = $this->Occurrences->Categories->find('list')->orderAsc('name');
        $this->set(compact('sectors', 'categories'));
    }

    public function changeStatus($id = null)
    {
        $occurrence = $this->Occurrences->get($id, [
            'contain' => ['Status']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $occurrence->status_id = $this->request->getData('status_id');
            if ($occurrence->status_id == DENIED) {
                $occurrence->observations = $this->request->getData('observations');
            } else {
                $occurrence->observations = null;
            }

            if ($this->Occurrences->save($occurrence)) {
                $this->Flash->success(__d('infrastructure', 'The status from occurrence has been changed'));
            } else {
                $this->Flash->error(__d('infrastructure', 'The status from occurrence could not be changed. Please, try again.'));
            }
            return $this->redirect(['plugin' => false, 'controller' => 'Pages', 'action' => 'home']);
        }
        $status = $this->Occurrences->Status->find('list')->orderAsc('name')->toArray();
        $this->set(compact('occurrence', 'status'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Occurrence id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $occurrence = $this->Occurrences->get($id);
        if ($this->Occurrences->delete($occurrence)) {
            $this->Flash->success(__d('infrastructure', 'The occurrence has been deleted.'));
        } else {
            $this->Flash->error(__d('infrastructure', 'The occurrence could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
