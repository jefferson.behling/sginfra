<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Form->postLink(__d('infrastructure', 'Delete'),
                            ['action' => 'delete', $category->id],
                            ['confirm' => __d('infrastructure', 'Are you sure you want to delete # {0}?', $category->id)]
                        ) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'List Categories'), ['action' => 'index']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Form->postLink('<i class="material-icons">delete</i>',
                        ['action' => 'delete', $category->id],
                        ['class' => 'btn-floating red tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'Delete Category'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'List Categories'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <?= $this->Form->create($category) ?>
                <span class="card-title green-text"><?= __d('infrastructure', 'Edit Category') ?></span>
                <div class="input-field required">
                    <?= $this->Form->control('name', ['label' => __d('infrastructure', 'Name')]) ?>
                </div>
                <?= $this->Form->button(__d('infrastructure', 'Submit'), ['class' => 'btn waves-effect green waves-light']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
