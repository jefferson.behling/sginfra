<div class="row">
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle tooltipped" data-position="left" data-tooltip="<?= __d('infrastructure', 'Actions') ?>">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">mode_edit</i>',
                        ['action' => 'edit', $sector->id],
                        ['class' => 'btn-floating green darken-4 tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'Edit Sector'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Form->postLink('<i class="material-icons">delete</i>',
                        ['action' => 'delete', $sector->id],
                        ['class' => 'btn-floating red tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'Delete Sector'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'List Sectors'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating blue tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'New Sector'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'Edit Sector'), ['action' => 'edit', $sector->id]) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Form->postLink(__d('infrastructure', 'Delete Sector'),
                        ['action' => 'delete', $sector->id]) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'List Sectors'), ['action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'New Sector'), ['action' => 'add']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= h($sector->name) ?></span>
                <table class="striped bordered">
                    <tbody>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Id') ?>
                            </td>
                            <td class="right">
                                <?= h($sector->id) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Name') ?>
                            </td>
                            <td class="right">
                                <?= h($sector->name) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
