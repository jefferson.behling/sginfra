<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('infrastructure', 'New Status'), ['action' => 'add']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating cyan tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'New Status'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text">
                    <?= __d('infrastructure', 'Status') ?>
                    <i class="material-icons left">linear_scale</i>
                </span>
                <?= $this->Form->create() ?>
                <div class="row">
                    <div class="input-field col s12">
                        <?= $this->Form->control('q', ['label' => __d('infrastructure', 'Filter by name'), 'value' => $q]) ?>
                    </div>
                </div>
                <div class="row">
                    <?= $this->Form->button(__d('infrastructure', 'Filter'), ['class' => 'btn waves-effect waves-green green col s10 offset-s1 m4 l2']) ?>
                </div>
                <?= $this->Form->end() ?>
                <div class="table-responsive-vertical shadow-z-1">
                    <table id="table" class="striped table table-hover table-mc-light-blue">
                        <thead>
                            <tr>
                                <th ><?= $this->Paginator->sort('name', ['label' => __d('infrastructure', 'Name')]) ?></th>
                                <th><?= __d('infrastructure', 'Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($status as $status): ?>
                            <tr>
                                <td data-title="<?= __d('infrastructure', 'Name') ?>"><?= $status->name != null ? h($status->name) : ' - ' ?></td>
                                <td data-title="<?= __d('infrastructure', 'Actions') ?>">
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('infrastructure', 'View') . '" >zoom_in</i>', ['action' => 'view', $status->id], ['escape' => false]) ?>
                                    <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __d('infrastructure', 'Edit') . '" >create</i>', ['action' => 'edit', $status->id], ['escape' => false]) ?>
                                    <?= $this->Form->postLink('<i class="material-icons tiny-custom black-text" title="'. __d('infrastructure', 'Delete') . '" >delete</i>', ['action' => 'delete', $status->id], ['confirm' => __d('infrastructure', 'Are you sure you want to delete # {0}?', $status->id), 'escape' => false]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="center">
            <ul class="pagination">
                <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
            </ul>
        </div>
        <p class="right"><?= $this->Paginator->counter(['format' => __d('infrastructure', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
