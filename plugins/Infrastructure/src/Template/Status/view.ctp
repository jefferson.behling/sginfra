<div class="row">
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle tooltipped" data-position="left" data-tooltip="<?= __d('infrastructure', 'Actions') ?>">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">mode_edit</i>',
                        ['action' => 'edit', $status->id],
                        ['class' => 'btn-floating green darken-4 tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'Edit Status'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'List Status'), 'escape' => false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating blue tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'New Status'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'List Status'), ['action' => 'index']) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'Edit Status'), ['action' => 'edit', $status->id]) ?>
                    </li>
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'New Status'), ['action' => 'add']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= h($status->name) ?></span>
                <table class="striped bordered">
                    <tbody>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Id') ?>
                            </td>
                            <td class="right">
                                <?= h($status->id) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Name') ?>
                            </td>
                            <td class="right">
                                <?= h($status->name) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
