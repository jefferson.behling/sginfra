<?php
    $this->layout = false;
?>
<?= $this->Form->create($occurrence, [
    'type' => 'post'
])
?>
<span class="card-title green-text"><?= __d('infrastructure', 'Change Status') ?></span>
<div class="row">
    <div class="col s12 m6 l6">
        <div class="input-field">
            <?= $this->Form->control('status_id', ['options' => $status, 'label' => __d('infrastructure', 'Status'), 'default' => $occurrence->status_id]) ?>
        </div>
    </div>
    <?php if ($occurrence->status_id == DENIED) { ?>
        <div class="input-field col s12" id="div-obs">
            <?= $this->Form->control('observations', ['label' => __d('infrastructure', 'Observations'), 'class' => 'materialize-textarea']) ?>
        </div>
    <?php } else { ?>
        <div class="input-field col s12 hide" id="div-obs">
            <?= $this->Form->control('observations', ['label' => __d('infrastructure', 'Observations'), 'class' => 'materialize-textarea']) ?>
        </div>
    <?php } ?>
</div>
<div class="row">
    <div class="col s12 m12 l12">
        <?= $this->Form->button(__d('infrastructure', 'Submit'), ['class' => 'btn green col s12 m3 l3 offset-m9 offset-l9']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('select').material_select();

        if ($("#observations").val() != '') {
            $("#observations + label").addClass('active');
        }
    });

    $("#status-id").change(function () {
       if ($(this).val() == '<?= DENIED ?>') {
            $("#div-obs").removeClass('hide');
            $("#observations").attr('required', true);
       } else {
           $("#div-obs").addClass('hide');
           $("#observations").attr('required', false);
           $("#observations").val('');
       }
    });
</script>

