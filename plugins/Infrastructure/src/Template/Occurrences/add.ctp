<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action">
                        <?= $this->Html->link(__d('infrastructure', 'List Occurrences'), ['action' => 'index']) ?>
                    </li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">list</i>',
                        ['action' => 'index'],
                        ['class' => 'btn-floating green tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'List Occurrences'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <?= $this->Form->create(null, ['type' => 'file']) ?>
                <span class="card-title green-text"><?= __d('infrastructure', 'Add Occurrence') ?></span>
                <div class="input-field">
                    <?= $this->Form->control('sector_id', ['label' => __d('infrastructure', 'Sector'), 'options' => $sectors]) ?>
                </div>
                <div class="input-field">
                    <?= $this->Form->control('category_id', ['label' => __d('infrastructure', 'Category'), 'options' => $categories]) ?>
                </div>
                <div class="input-field required">
                    <?= $this->Form->control('title', ['label' => __d('infrastructure', 'Title'), 'required']) ?>
                </div>
                <div class="input-field">
                    <?= $this->Form->control('locale', ['label' => __d('infrastructure', 'Locale')]) ?>
                </div>
                <div class="input-field">
                    <?= $this->Form->control('description', ['label' => __d('infrastructure', 'Description'), 'class' => 'materialize-textarea']) ?>
                </div>
                <div class="file-field input-field">
                    <div class="btn orange lighten-2">
                        <?= $this->Form->control('image', ['type' => 'file', 'label' => false, 'required']) ?>
                        <i class="material-icons left">add_photo_alternate</i>
                        <span for="image"><?= __d('infrastructure', 'Image') ?></span>
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
                <?= $this->Form->button(__d('infrastructure', 'Submit'), ['class' => 'btn waves-effect green waves-light']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
