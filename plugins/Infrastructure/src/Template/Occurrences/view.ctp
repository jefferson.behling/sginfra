<div class="row">
    <div class="col s12 m12 l10 offset-l1">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= h($occurrence->title) ?></span>
                <table class="striped bordered">
                    <tbody>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Title') ?>
                            </td>
                            <td class="right">
                                <?= h($occurrence->title) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Locale') ?>
                            </td>
                            <td class="right">
                                <?= h($occurrence->locale) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'User') ?>
                            </td>
                            <td class="right">
                                <?= $occurrence->has('user') ? $this->Html->link($occurrence->user->name, ['plugin' => 'Accounts/Auth', 'controller' => 'Users', 'action' => 'view', $occurrence->user->id]) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Sector') ?>
                            </td>
                            <td class="right">
                                <?= $occurrence->has('sector') ? $this->Html->link($occurrence->sector->name, ['controller' => 'Sectors', 'action' => 'view', $occurrence->sector->id]) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Status') ?>
                            </td>
                            <td class="right">
                                <?= $occurrence->has('status') ? $this->Html->link($occurrence->status->name, ['controller' => 'Status', 'action' => 'view', $occurrence->status->id]) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Category') ?>
                            </td>
                            <td class="right">
                                <?= $occurrence->has('category') ? $this->Html->link($occurrence->category->name, ['controller' => 'Categories', 'action' => 'view', $occurrence->category->id]) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Created') ?>
                            </td>
                            <td class="right">
                                <?= h($occurrence->created->format('d/m/Y H:i')) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Modified') ?>
                            </td>
                            <td class="right">
                                <?= h($occurrence->modified->format('d/m/Y H:i')) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= __d('infrastructure', 'Description') ?>
                            </td>
                            <td class="right">
                                <?= $occurrence->description != null ? $this->Text->autoParagraph(h($occurrence->description)) : ' - '; ?>
                            </td>
                        </tr>
                        <?php if ($occurrence->status_id == DENIED) { ?>
                            <tr>
                                <td>
                                    <?= __d('infrastructure', 'Observations') ?>
                                </td>
                                <td class="right">
                                    <?= $this->Text->autoParagraph(h($occurrence->observations)); ?>
                                </td>
                            </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <h4><?= __d('infrastructure', 'Image') ?></h4>
                <div class="col s12">
                    <?= $this->Html->image('occurrences/'. $occurrence->image, [
                        'class' => 'materialboxed z-depth-2', 'height' => '400'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
