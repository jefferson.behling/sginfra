<style>
    .badge.change-status:hover {
        cursor: pointer;
    }

    .collection.user-info {
        border: none !important;
    }

</style>
<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('infrastructure', 'New Occurrence'), ['action' => 'add']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating cyan tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'New Occurrence'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12 m12 l9">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Occurrences') ?></span>
                <div class="col s12">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">photo_camera</i>
                                <?= __d('infrastructure', 'Did you find a problem?') ?>
                            </div>
                            <div class="collapsible-body">
                                <span>
                                    <?= $this->Form->create(null, ['type' => 'file']) ?>
                                    <div class="table-responsive-vertical shadow-z-1">
                                        <div class="input-field required">
                                            <?= $this->Form->control('title', ['label' => __d('infrastructure', 'Title'), 'required']) ?>
                                        </div>
                                        <div class="input-field">
                                            <?= $this->Form->control('locale', ['label' => __d('infrastructure', 'Locale')]) ?>
                                        </div>
                                        <div class="input-field">
                                            <?= $this->Form->control('description', ['label' => __d('infrastructure', 'Description'), 'class' => 'materialize-textarea']) ?>
                                        </div>
                                        <div class="file-field input-field">
                                            <div class="btn orange lighten-2">
                                                <?= $this->Form->control('image', ['type' => 'file', 'label' => false, 'required']) ?>
                                                <i class="material-icons left">add_photo_alternate</i>
                                                <span for="image"><?= __d('infrastructure', 'Image') ?></span>
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <?= $this->Form->control('sector_id', ['label' => __d('infrastructure', 'Sector'), 'options' => $sectors]) ?>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <?= $this->Form->control('category_id', ['label' => __d('infrastructure', 'Category'), 'options' => $categories]) ?>
                                        </div>
                                        <?= $this->Form->button(__d('infrastructure', 'Submit'), ['class' => 'btn waves-effect green waves-light']) ?>
                                    </div>
                                    <?= $this->Form->end() ?>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
                <?php foreach ($occurrences as $occurrence): ?>
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card occurrence darken-1 col s12">
                            <div class="card-content black-text">
                                <div class="table-responsive-vertical shadow-z-1">
                                    <div class="row">
                                        <div class="collection user-info">
                                            <div class="collection-item avatar">
                                                <?php
                                                $email_hash = md5(strtolower(trim($occurrence->user->email)));
                                                ?>
                                                <?= $this->Html->image(
                                                    'https://www.gravatar.com/avatar/' . $email_hash . "?s=400",
                                                    ['alt' => $occurrence->user->name, 'title' => $occurrence->user->name, 'height' => '40', 'width' => '40', 'data-beloworigin' => 'true', 'class' => 'prefix circle']
                                                ) ?>
                                                <span><?= $occurrence->user->name ?></span><br>
                                                <span class="time-ago"><?= $occurrence->created->diffForHumans() ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12" align="center">
                                            <?= $this->Html->image('occurrences/'. $occurrence->image, [
                                                'class' => 'materialboxed responsive-img z-depth-2'
                                            ]) ?>
                                        </div>
                                        <div class="col s12">
                                            <h5><?= $occurrence->title ?></h5>
                                        </div>
                                        <div class="col s12">
                                            <?= $occurrence->locale != null ? $occurrence->locale . ', ' . $occurrence->sector->name : $occurrence->sector->name ?>
                                            <i class="material-icons tiny-custom left">location_on</i>
                                        </div>
                                        <div class="hide-on-large-only">
                                            <div class="col s12 m12 l12">
                                                <?php if ($occurrence->status_id != DENIED) { ?>
                                                    <?= $occurrence->status->name ?>
                                                    <i class="material-icons tiny-custom left">linear_scale</i>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php if ($occurrence->status_id == SENT) { ?>
                                            <ul class="timeline" id="timeline">
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Sent') ?>">
                                                    </div>
                                                </li>
                                                <li class="li">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Received') ?>">
                                                    </div>
                                                </li>
                                                <li class="li">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'In Progress') ?>">
                                                    </div>
                                                </li>
                                                <li class="li">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Finished') ?>">
                                                    </div>
                                                </li>
                                            </ul>
                                        <?php } else if ($occurrence->status_id == RECEIVED) { ?>
                                            <ul class="timeline" id="timeline">
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Sent') ?>">
                                                    </div>
                                                </li>
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Received') ?>">
                                                    </div>
                                                </li>
                                                <li class="li">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'In Progress') ?>">
                                                    </div>
                                                </li>
                                                <li class="li">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Finished') ?>">
                                                    </div>
                                                </li>
                                            </ul>
                                        <?php } else if ($occurrence->status_id == IN_PROGRESS) { ?>
                                            <ul class="timeline" id="timeline">
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Sent') ?>">
                                                    </div>
                                                </li>
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Received') ?>">
                                                    </div>
                                                </li>
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'In Progress') ?>">
                                                    </div>
                                                </li>
                                                <li class="li">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Finished') ?>">
                                                    </div>
                                                </li>
                                            </ul>
                                        <?php } else if ($occurrence->status_id == FINISHED) { ?>
                                            <ul class="timeline" id="timeline">
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Sent') ?>">
                                                    </div>
                                                </li>
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Received') ?>">
                                                    </div>
                                                </li>
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'In Progress') ?>">
                                                    </div>
                                                </li>
                                                <li class="li complete">
                                                    <div class="status tooltipped" data-position="top" data-delay="50" data-tooltip="<?= __d('infrastructure', 'Finished') ?>">
                                                    </div>
                                                </li>
                                            </ul>
                                        <?php } else { ?>
                                            <span class="badge red white-text left" data-badge-caption="<?= __d('infrastructure', 'Denied') ?>"></span>
                                            <br><br><p><b><?= __d('infrastructure', 'Reason: ') ?></b><?= $occurrence->observations ?></p>
                                        <?php } ?>
                                        <?php if (in_array($profile_role, [ROLE_ADMIN, ROLE_INFRA])) { ?>
                                            <span class="badge new white-text change-status" onclick="changeStatus('<?= $occurrence->id ?>')" data-badge-caption="<?= __d('infrastructure', 'Change Status') ?>"></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="center">
                <ul class="pagination">
                    <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                    <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                    <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                </ul>
                <p class="right"><?= $this->Paginator->counter(['format' => __d('infrastructure', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
<div class="modal">
    <div class="modal-content">
        <div class="preloader-wrapper small">
            <div class="spinner-layer spinner-green-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <div id="datas"></div>
    </div>
</div>
<script>
    function changeStatus(id) {
        $.ajax({
            url: '<?= $this->Url->build(['action' => 'change-status']) ?>' + '/' + id,
            beforeSend: function() {
                $("#datas").html('');
                $(".modal").modal('open');
                $(".preloader-wrapper").addClass('active');
            }
        })
            .done(function(data) {
                $(".preloader-wrapper").removeClass('active');
                $("#datas").html(data);
            })
    }
</script>