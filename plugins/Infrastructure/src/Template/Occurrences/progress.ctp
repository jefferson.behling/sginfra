<style>
    .badge.change-status:hover {
        cursor: pointer;
    }
</style>
<div class="row">
    <div class="col s12 m3 hide-on-med-and-down">
        <div class="card darken-1">
            <div class="card-content white-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Actions') ?></span>
                <div class="card-action">
                    <li class="padding-action"><?= $this->Html->link(__d('infrastructure', 'New Occurrence'), ['action' => 'add']) ?></li>
                </div>
            </div>
        </div>
    </div>
    <div class="hide-on-large-only">
        <div class="fixed-action-btn click-to-toggle">
            <a class="btn-floating btn-large red">
                <i class="large material-icons">dashboard</i>
            </a>
            <ul>
                <li>
                    <?= $this->Html->link('<i class="material-icons">add</i>',
                        ['action' => 'add'],
                        ['class' => 'btn-floating cyan tooltipped', 'data-position' => 'left', 'data-tooltip' =>  __d('infrastructure', 'New Occurrence'), 'escape' => false]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col s12 m12 l9">
        <div class="card darken-1 col s12 m12 l9">
            <div class="card-content black-text">
                <span class="card-title green-text"><?= __d('infrastructure', 'Occurrences - Progress') ?></span>
                <?php foreach ($occurrences as $occurrence): ?>
                <div class="col s12 m12 l12">
                    <div class="card darken-1 col s12">
                        <div class="card-content black-text">
                            <div class="table-responsive-vertical shadow-z-1">
                                <div class="row">
                                    <div class="col s1">
                                        <?php
                                        $email_hash = md5(strtolower(trim($occurrence->user->email)));
                                        ?>
                                        <?= $this->Html->image(
                                            'https://www.gravatar.com/avatar/' . $email_hash . "?s=40",
                                            ['alt' => $occurrence->user->name, 'title' => $occurrence->user->name, 'data-beloworigin' => 'true', 'class' => 'circle']
                                        ) ?>
                                    </div>
                                    <div class="col s10">
                                        <?= $occurrence->user->name ?><br>
                                        <span class="time-ago"><?= $occurrence->created->diffForHumans() ?></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12" align="center">
                                        <?= $this->Html->image('occurrences/'. $occurrence->image, [
                                            'class' => 'materialboxed z-depth-2', 'height' => '400'
                                        ]) ?>
                                    </div>
                                    <div class="col s12">
                                        <h5><?= $occurrence->title ?></h5>
                                    </div>
                                    <div class="col s12">
                                        <?= $occurrence->locale != null ? $occurrence->locale . ', ' . $occurrence->sector->name : $occurrence->sector->name ?>
                                        <i class="material-icons tiny-custom left">location_on</i>
                                    </div>
                                    <?php if (in_array($profile_role, [ROLE_ADMIN, ROLE_INFRA])) { ?>
                                        <span class="badge new white-text change-status" onclick="changeStatus('<?= $occurrence->id ?>')" data-badge-caption="<?= __d('infrastructure', 'Change Status') ?>"></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <div class="center">
                        <ul class="pagination">
                            <?= $this->Paginator->first('<i class="material-icons">first_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                            <?= $this->Paginator->prev('<i class="material-icons">chevron_left</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('<i class="material-icons">chevron_right</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                            <?= $this->Paginator->last('<i class="material-icons">last_page</i>', ['class' => 'waves-effect', 'escape' => false]) ?>
                        </ul>
                        <p class="right"><?= $this->Paginator->counter(['format' => __d('infrastructure', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal">
    <div class="modal-content">
        <div class="preloader-wrapper small">
            <div class="spinner-layer spinner-green-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <div id="datas"></div>
    </div>
</div>
<script>
    function changeStatus(id) {
        $.ajax({
            url: '<?= $this->Url->build(['action' => 'change-status']) ?>' + '/' + id,
            beforeSend: function() {
                $("#datas").html('');
                $(".modal").modal('open');
                $(".preloader-wrapper").addClass('active');
            }
        })
            .done(function(data) {
                $(".preloader-wrapper").removeClass('active');
                $("#datas").html(data);
            })
    }
</script>