<?php
namespace Infrastructure\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sectors Model
 *
 * @property \Infrastructure\Model\Table\OccurrencesTable|\Cake\ORM\Association\HasMany $Occurrences
 *
 * @method \Infrastructure\Model\Entity\Sector get($primaryKey, $options = [])
 * @method \Infrastructure\Model\Entity\Sector newEntity($data = null, array $options = [])
 * @method \Infrastructure\Model\Entity\Sector[] newEntities(array $data, array $options = [])
 * @method \Infrastructure\Model\Entity\Sector|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infrastructure\Model\Entity\Sector patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Infrastructure\Model\Entity\Sector[] patchEntities($entities, array $data, array $options = [])
 * @method \Infrastructure\Model\Entity\Sector findOrCreate($search, callable $callback = null, $options = [])
 */
class SectorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sectors');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Tree');

        $this->hasMany('Occurrences', [
            'foreignKey' => 'sector_id',
            'className' => 'Infrastructure.Occurrences'
        ]);

        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('q', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $values = explode(' ', trim($args['q']));
                    foreach ($values as $value) {
                        $query->where(["Sectors.name LIKE" => "%$value%"]);
                    }
                }
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
}
