<?php
namespace Infrastructure\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categories Model
 *
 * @property \Infrastructure\Model\Table\OccurrencesTable|\Cake\ORM\Association\HasMany $Occurrences
 *
 * @method \Infrastructure\Model\Entity\Category get($primaryKey, $options = [])
 * @method \Infrastructure\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \Infrastructure\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \Infrastructure\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infrastructure\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Infrastructure\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \Infrastructure\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class CategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Occurrences', [
            'foreignKey' => 'category_id',
            'className' => 'Infrastructure.Occurrences'
        ]);

        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('q', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    $values = explode(' ', trim($args['q']));
                    foreach ($values as $value) {
                        $query->where(["Categories.name LIKE" => "%$value%"]);
                    }
                }
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
}
