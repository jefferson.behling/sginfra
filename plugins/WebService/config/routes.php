<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'WebService',
    ['path' => '/web-service'],
    function (RouteBuilder $routes) {
        $routes->setExtensions(['json']);
        $routes->fallbacks(DashedRoute::class);
    }
);
