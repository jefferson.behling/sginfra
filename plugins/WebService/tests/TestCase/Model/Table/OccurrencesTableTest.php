<?php
namespace WebService\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use WebService\Model\Table\OccurrencesTable;

/**
 * WebService\Model\Table\OccurrencesTable Test Case
 */
class OccurrencesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \WebService\Model\Table\OccurrencesTable
     */
    public $Occurrences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.web_service.occurrences',
        'plugin.web_service.users',
        'plugin.web_service.sectors',
        'plugin.web_service.status',
        'plugin.web_service.categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Occurrences') ? [] : ['className' => OccurrencesTable::class];
        $this->Occurrences = TableRegistry::get('Occurrences', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Occurrences);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
