<?php
namespace WebService\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use WebService\Controller\Component\JWTManagerComponent;

/**
 * WebService\Controller\Component\JWTManagerComponent Test Case
 */
class JWTManagerComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \WebService\Controller\Component\JWTManagerComponent
     */
    public $JWTManager;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->JWTManager = new JWTManagerComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JWTManager);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
