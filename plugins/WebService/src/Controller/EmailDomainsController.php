<?php
namespace WebService\Controller;

use WebService\Controller\AppController;

/**
 * EmailDomains Controller
 *
 * @property \WebService\Model\Table\EmailDomainsTable $EmailDomains
 *
 * @method \WebService\Model\Entity\EmailDomain[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmailDomainsController extends AppController
{
    public function getEmailDomains()
    {
        $emailDomains = $this->EmailDomains->find()
            ->where(['active' => true])
            ->orderAsc('domain');

        $this->set([
            'email_domains' => $emailDomains,
            '_serialize' => ['email_domains']
        ]);
    }
}
