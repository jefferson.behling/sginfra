<?php

namespace WebService\Controller;

use App\Controller\AppController as BaseController;
use Cake\Network\Exception\UnauthorizedException;

class AppController extends BaseController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');

        $actionsAllowed = ['login', 'register', 'forgotPassword', 'recoveryPass', 'activateAccount', 'getEmailDomains'];

        if (!in_array($this->request->getParam('action'), $actionsAllowed)) {
            $this->loadModel('WebService.Users');
            $id = $this->request->getData('auth');
            $user = $this->Users->findById($id);

            if ($user->count() != 0) {
                $user = $this->Users->get($id);
                $this->Auth->setUser($user);
            } else {
                throw new UnauthorizedException(__d('web_service', 'You are not authorized to access that location.'));
            }
        }

    }
}