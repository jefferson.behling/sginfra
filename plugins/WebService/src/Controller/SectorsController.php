<?php
namespace WebService\Controller;

use WebService\Controller\AppController;

/**
 * Sectors Controller
 *
 *
 * @method \WebService\Model\Entity\Sector[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SectorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $sectors = $this->Sectors->find()
            ->orderAsc('name');

        $sectors = $this->paginate($sectors);

        $this->set([
            'sectors' => $sectors,
            '_serialize' => ['sectors']
        ]);
    }
}
