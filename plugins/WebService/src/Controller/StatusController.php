<?php
namespace WebService\Controller;

use WebService\Controller\AppController;

/**
 * Status Controller
 *
 * @property \WebService\Model\Table\StatusTable $Status
 *
 * @method \WebService\Model\Entity\Status[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StatusController extends AppController
{
    /**
     * Index method
     */
    public function index()
    {
        $status = $this->paginate($this->Status);

        $this->set([
            'status' => $status,
            '_serialize' => ['status']
        ]);
    }
}
