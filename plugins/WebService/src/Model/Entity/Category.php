<?php
namespace WebService\Model\Entity;

use Cake\ORM\Entity;

/**
 * Category Entity
 *
 * @property string $id
 * @property string $name
 * @property string $parent_id
 * @property int $lft
 * @property int $rght
 *
 * @property \WebService\Model\Entity\ParentCategory $parent_category
 * @property \WebService\Model\Entity\ChildCategory[] $child_categories
 * @property \WebService\Model\Entity\Occurrence[] $occurrences
 */
class Category extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'parent_category' => true,
        'child_categories' => true,
        'occurrences' => true
    ];
}
