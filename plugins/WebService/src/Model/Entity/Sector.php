<?php
namespace WebService\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sector Entity
 *
 * @property string $id
 * @property string $name
 * @property string $parent_id
 * @property int $lft
 * @property int $rght
 *
 * @property \WebService\Model\Entity\ParentSector $parent_sector
 * @property \WebService\Model\Entity\Occurrence[] $occurrences
 * @property \WebService\Model\Entity\ChildSector[] $child_sectors
 */
class Sector extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'parent_sector' => true,
        'occurrences' => true,
        'child_sectors' => true
    ];
}
