<?php
namespace WebService\Model\Entity;

use Cake\ORM\Entity;

/**
 * Occurrence Entity
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $locale
 * @property string $image
 * @property string $observations
 * @property string $user_id
 * @property string $sector_id
 * @property string $status_id
 * @property string $category_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \WebService\Model\Entity\User $user
 * @property \WebService\Model\Entity\Sector $sector
 * @property \WebService\Model\Entity\Status $status
 * @property \WebService\Model\Entity\Category $category
 */
class Occurrence extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'description' => true,
        'locale' => true,
        'image' => true,
        'observations' => true,
        'user_id' => true,
        'sector_id' => true,
        'status_id' => true,
        'category_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'sector' => true,
        'status' => true,
        'category' => true
    ];
}
