<?php
namespace WebService\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sectors Model
 *
 * @property \WebService\Model\Table\SectorsTable|\Cake\ORM\Association\BelongsTo $ParentSectors
 * @property \WebService\Model\Table\OccurrencesTable|\Cake\ORM\Association\HasMany $Occurrences
 * @property \WebService\Model\Table\SectorsTable|\Cake\ORM\Association\HasMany $ChildSectors
 *
 * @method \WebService\Model\Entity\Sector get($primaryKey, $options = [])
 * @method \WebService\Model\Entity\Sector newEntity($data = null, array $options = [])
 * @method \WebService\Model\Entity\Sector[] newEntities(array $data, array $options = [])
 * @method \WebService\Model\Entity\Sector|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WebService\Model\Entity\Sector patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WebService\Model\Entity\Sector[] patchEntities($entities, array $data, array $options = [])
 * @method \WebService\Model\Entity\Sector findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class SectorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sectors');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Tree');

        $this->belongsTo('ParentSectors', [
            'className' => 'WebService.Sectors',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('Occurrences', [
            'foreignKey' => 'sector_id',
            'className' => 'WebService.Occurrences'
        ]);
        $this->hasMany('ChildSectors', [
            'className' => 'WebService.Sectors',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentSectors'));

        return $rules;
    }
}
