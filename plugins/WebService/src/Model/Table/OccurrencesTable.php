<?php
namespace WebService\Model\Table;

use Cake\Core\App;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Network\Exception\NotImplementedException;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * Occurrences Model
 *
 * @property \WebService\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \WebService\Model\Table\SectorsTable|\Cake\ORM\Association\BelongsTo $Sectors
 * @property \WebService\Model\Table\StatusTable|\Cake\ORM\Association\BelongsTo $Status
 * @property \WebService\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 *
 * @method \WebService\Model\Entity\Occurrence get($primaryKey, $options = [])
 * @method \WebService\Model\Entity\Occurrence newEntity($data = null, array $options = [])
 * @method \WebService\Model\Entity\Occurrence[] newEntities(array $data, array $options = [])
 * @method \WebService\Model\Entity\Occurrence|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WebService\Model\Entity\Occurrence patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WebService\Model\Entity\Occurrence[] patchEntities($entities, array $data, array $options = [])
 * @method \WebService\Model\Entity\Occurrence findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OccurrencesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('occurrences');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'Infrastructure.Users'
        ]);
        $this->belongsTo('Sectors', [
            'foreignKey' => 'sector_id',
            'joinType' => 'INNER',
            'className' => 'Infrastructure.Sectors'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER',
            'className' => 'Infrastructure.Status'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
            'className' => 'Infrastructure.Categories'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 255)
            ->allowEmpty('locale');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        $validator
            ->scalar('observations')
            ->allowEmpty('observations');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['sector_id'], 'Sectors'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, $options = array())
    {
        if(!empty($entity['image']['name'])) {
            $entity['image'] = $this->upload($entity['image']);
        } else {
            unset($entity['image']);
        }
    }

    public function upload($image, $dir = 'img/occurrences')
    {
        $dir = WWW_ROOT.$dir.DS;

        if (($image['error'] != 0) && ($image['size'] == 0)) {
            throw new NotImplementedException('Falha ao fazer Upload da imagem.');
        }

        $this->checkDirectory($dir);

        $image = $this->checkName($image, $dir);

        $this->moveFiles($image, $dir);

        return $image['name'];
    }

    public function checkDirectory($dir = 'img/occurrences')
    {
        $folder = new Folder();
        if (!is_dir($dir)){
            $folder->create($dir);
        }
    }

    public function checkName($image, $dir)
    {
        $image_info = pathinfo($dir.$image['name']);
        $image_name = $this->manipulateName($image_info['filename']).'.'.$image_info['extension'];
        $count = 2;
        while (file_exists($dir.$image_name)) {
            $image_name  = $this->manipulateName($image_info['filename']).'-'.$count;
            $image_name .= '.'.$image_info['extension'];
            $count++;
        }
        $image['name'] = $image_name;
        return $image;
    }

    public function manipulateName($image_name)
    {
        $image_name = strtolower(Text::slug($image_name,'-'));
        return md5($image_name . date('d/m/Y H:i:s:u'));
    }

    public function moveFiles($image, $dir)
    {
        $arquivo = new File($image['tmp_name']);
        $arquivo->copy($dir.$image['name']);
        chmod($dir.$image['name'], 0777);
        $arquivo->close();
    }

    public function getRecords($entity)
    {
        $occurrence = $this->newEntity();
        $occurrence->title = $entity['title'];
        $occurrence->locale = $entity['locale'];
        $occurrence->description = $entity['description'];
        $occurrence->sector_id = $entity['sector_id'];
        $occurrence->category_id = $entity['category_id'];
        $occurrence->status_id = RECEIVED;
        $occurrence->image = $entity['image'];

        return $occurrence;
    }
}