<?php
namespace WebService\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Status Model
 *
 * @property \WebService\Model\Table\OccurrencesTable|\Cake\ORM\Association\HasMany $Occurrences
 *
 * @method \WebService\Model\Entity\Status get($primaryKey, $options = [])
 * @method \WebService\Model\Entity\Status newEntity($data = null, array $options = [])
 * @method \WebService\Model\Entity\Status[] newEntities(array $data, array $options = [])
 * @method \WebService\Model\Entity\Status|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \WebService\Model\Entity\Status patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \WebService\Model\Entity\Status[] patchEntities($entities, array $data, array $options = [])
 * @method \WebService\Model\Entity\Status findOrCreate($search, callable $callback = null, $options = [])
 */
class StatusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('status');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Occurrences', [
            'foreignKey' => 'status_id',
            'className' => 'WebService.Occurrences'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
