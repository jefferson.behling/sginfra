<?php
/**
 * Created by Jefferson Vantuir
 * https://jeffersonbehling.github.io/
 * jefferson.behling@gmail.com
 */

$description = 'SGInfra';
?>
<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $description ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon', 'icon.ico') ?>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <?= $this->Html->css('Materialize.materialize.min.css') ?>
    <?= $this->Html->css('Materialize.materialize.css') ?>
    <?= $this->Html->css('Materialize.custom.css') ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<!--Import jQuery before materialize.js-->
<?= $this->Html->script('jquery-3.2.1.min.js') ?>
<?= $this->Html->script('materialize.min.js') ?>
<?= $this->fetch('meta') ?>
<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>

<body class="body-bg preloader">
<?= $this->Flash->render() ?>
<div class=" margin-top-5">
    <?= $this->fetch('content') ?>
</div>
<footer>
</footer>
</body>
</html>
