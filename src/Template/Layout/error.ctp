<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('materialize.min.css') ?>
    <?= $this->Html->css('custom.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <style>
        .logo-error {
            -webkit-filter: grayscale(100%);
            -moz-filter: grayscale(100%);
            -ms-filter: grayscale(100%);
            -o-filter: grayscale(100%);
            filter: grayscale(100%);
            margin-top: 2rem;
        }

        .no-hover:hover {
            color: #fff !important;
        }
    </style>
</head>

<body>
<div class="row">
    <div class="col s12 center">
        <?= $this->Html->image('logo.png', ['class' => 'logo-error', 'height' => '200']) ?>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
        <?= $this->Html->link(__('Página Inicial'), ['plugin' => null, 'controller' => 'Users', 'action' => 'login'], ['class' => 'btn waves-effect waves-green green white-text no-hover']) ?>
    </div>
</div>
</body>
</html>