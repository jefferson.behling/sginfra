<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'SGInfra';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon', 'icon.ico') ?>

    <?= $this->Html->css('Materialize.materialize.min.css') ?>
    <?= $this->Html->css('Materialize.custom.css') ?>
    <?= $this->Html->css('Materialize.responsive-table.css'); ?>
    <?= $this->Html->css('Materialize.horizontal-timeline.css'); ?>

    <?= $this->Html->script('Materialize.jquery-3.2.1.min.js') ?>
    <?= $this->Html->script('Materialize.materialize.min.js') ?>
    <?= $this->Html->script('Materialize.init.js') ?>
    <?= $this->Html->script('Materialize.responsive-table.js') ?>
    <?= $this->Html->script('Materialize.initialize.js') ?>
    <?= $this->Html->script('Materialize.horizontal-timeline.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="body-bg">
    <main>
        <?= $this->element('navbar') ?>
        <?= $this->Flash->render() ?>
        <?= $this->element('sidenav') ?>
        <?= $this->fetch('content') ?>
    </main>

    <?= $this->element('footer') ?>
</body>
</html>
<script>
    $(document).ready(function() {
        $('select').material_select();
        $('.modal').modal();
    });

    $('.datepicker').pickadate({
        selectMonths: false, // Creates a dropdown to control month
        selectYears: 200, // Creates a dropdown of 15 years to control year,
        monthsFull: [
            "<?= __d('materialize', 'January') ?>",
            "<?= __d('materialize', 'February') ?>",
            "<?= __d('materialize', 'March') ?>",
            "<?= __d('materialize', 'April') ?>",
            "<?= __d('materialize', 'May') ?>",
            "<?= __d('materialize', 'June') ?>",
            "<?= __d('materialize', 'July') ?>",
            "<?= __d('materialize', 'August') ?>",
            "<?= __d('materialize', 'September') ?>",
            "<?= __d('materialize', 'October') ?>",
            "<?= __d('materialize', 'November') ?>",
            "<?= __d('materialize', 'December') ?>"],
        monthsShort: [
            "<?= __d('materialize', 'Jan') ?>",
            "<?= __d('materialize', 'Fev') ?>",
            "<?= __d('materialize', 'Mar') ?>",
            "<?= __d('materialize', 'Apr') ?>",
            "<?= __d('materialize', 'May') ?>",
            "<?= __d('materialize', 'Jun') ?>",
            "<?= __d('materialize', 'Jul') ?>",
            "<?= __d('materialize', 'Aug') ?>",
            "<?= __d('materialize', 'Sep') ?>",
            "<?= __d('materialize', 'Oct') ?>",
            "<?= __d('materialize', 'Nov') ?>",
            "<?= __d('materialize', 'Dec') ?>"],
        weekdaysFull: [
            "<?= __d('materialize', 'Sunday') ?>",
            "<?= __d('materialize', 'Monday') ?>",
            "<?= __d('materialize', 'Tuesday') ?>",
            "<?= __d('materialize', 'Wednesday') ?>",
            "<?= __d('materialize', 'Thursday') ?>",
            "<?= __d('materialize', 'Friday') ?>",
            "<?= __d('materialize', 'Saturday') ?>"],
        weekdaysShort: [
            "<?= __d('materialize', 'Sun') ?>",
            "<?= __d('materialize', 'Mon') ?>",
            "<?= __d('materialize', 'Tue') ?>",
            "<?= __d('materialize', 'Wed') ?>",
            "<?= __d('materialize', 'Thu') ?>",
            "<?= __d('materialize', 'Fri') ?>",
            "<?= __d('materialize', 'Sat') ?>"],
        weekdaysLetter: [
            "<?= __d('materialize', 'S') ?>",
            "<?= __d('materialize', 'M') ?>",
            "<?= __d('materialize', 'T') ?>",
            "<?= __d('materialize', 'W') ?>",
            "<?= __d('materialize', 'T') ?>",
            "<?= __d('materialize', 'F') ?>",
            "<?= __d('materialize', 'S') ?>"],
        today: "<?= __d('materialize', 'Today') ?>",
        clear: "<?= __d('materialize', 'Clear') ?>",
        close: 'Ok',
        closeOnSelect: true, // Close upon selecting a date,
        format: 'dd/mm/yyyy'
    });

    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: "<?= __d('materialize', 'Clear') ?>", // text for clear-button
        canceltext: "<?= __d('materialize', 'Cancel') ?>", // Text for cancel-button
        format: "HH:ii",
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
    });

</script>
