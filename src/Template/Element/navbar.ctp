<nav class="green-background">
    <div class="nav-wrapper">
        <a href="#" data-activates="slide-out" class="button-collapse show-on-large"><i class="material-icons">menu</i></a>
        <?= $this->Html->link('SGInfra', ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'index'],
            ['class' => 'brand-logo center']) ?>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><?= $this->Html->link(__('Logout'), ['plugin' => null, 'controller' => 'Users', 'action' => 'logout']) ?></li>
        </ul>
    </div>
</nav>