<ul id="slide-out" class="side-nav">
    <li>
        <div class="user-view">
            <div class="background">
                <?= $this->Html->image('back.jpg') ?>
            </div>
            <a href="#!user">
                <?php
                    $email_hash = md5(strtolower(trim($profile_email)));
                ?>
                <?= $this->Html->image(
                    'https://www.gravatar.com/avatar/' . $email_hash . "?s=40",
                    ['alt' => 'Sua conta', 'title' => 'Sua conta', 'data-beloworigin' => 'true', 'class' => 'circle']
                ) ?>
            </a>
            <a href="#!name"><span class="white-text name"><?= $profile_name ?></span></a>
            <a href="#!email"><span class="white-text email"><?= $profile_email ?></span></a>
        </div>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
            <li>
                <div class="collapsible-header">
                    <i class="material-icons">person</i>
                    <?= __('Profile') ?>
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <?= $this->Html->link('<i class="material-icons left">person</i>' . __('Profile'), [
                                'plugin' => 'Accounts/Profile', 'controller' => 'Users', 'action' => 'index'
                            ], ['escape' => false]
                            ) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="material-icons left">lock</i>' . __('Change Password'), [
                                'plugin' => 'Accounts/Profile', 'controller' => 'Users', 'action' => 'change-password'
                            ], ['escape' => false]
                            ) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="material-icons left">exit_to_app</i>' . __('Logout'), [
                                'plugin' => null, 'controller' => 'Users', 'action' => 'logout'
                            ], ['escape' => false]
                            ) ?>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <?php if (in_array($profile_role, [ROLE_INFRA, ROLE_ADMIN])) { ?>
            <?php if (in_array($profile_role, [ROLE_ADMIN])) { ?>
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <div class="collapsible-header">
                            <i class="material-icons">settings</i>
                            <?= __('Configurations') ?>
                        </div>
                        <div class="collapsible-body">
                            <ul>
                                <li>
                                    <?= $this->Html->link('<i class="material-icons left">gavel</i>' . __('Permissions'),
                                        ['plugin' => 'Accounts/Permissions', 'controller' => 'Permissions', 'action' => 'index'],
                                        ['escape' => false]) ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('<i class="material-icons left">alternate_email</i>' . __('Email Domains'),
                                        ['plugin' => 'Accounts/Admin', 'controller' => 'EmailDomains', 'action' => 'index'],
                                        ['escape' => false]) ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('<i class="material-icons left">person</i>' . __('Users'),
                                        ['plugin' => 'Accounts/Admin', 'controller' => 'Users', 'action' => 'index'],
                                        ['escape' => false]) ?>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            <?php } ?>
            <ul class="collapsible collapsible-accordion">
                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">add_alert</i>
                        <?= __('Occurrences') ?>
                    </div>
                    <div class="collapsible-body">
                        <ul>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">dashboard</i>' . __('Dashboard'),
                                    ['plugin' => null, 'controller' => 'Pages', 'action' => 'home'],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">view_agenda</i>' . __('Home'),
                                    ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'index'],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">new_releases</i>' . __('News'),
                                    ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'news'],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">hourglass_full</i>' . __('In Progress'),
                                    ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'progress'],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">check</i>' . __('Finished'),
                                    ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'finished'],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">close</i>' . __('Denied'),
                                    ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'denied'],
                                    ['escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <ul class="collapsible collapsible-accordion">
                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">timeline</i>
                        <?= __('Reports') ?>
                    </div>
                    <div class="collapsible-body">
                        <ul>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">developer_board</i>' . __('Full'),
                                    ['plugin' => 'Reports', 'controller' => 'Occurrences', 'action' => 'index'],
                                    ['escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <ul class="collapsible collapsible-accordion">
                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">add</i>
                        <?= __('Register') ?>
                    </div>
                    <div class="collapsible-body">
                        <ul>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">category</i>' . __('Categories'), [
                                    'plugin' => 'Infrastructure', 'controller' => 'Categories', 'action' => 'index'
                                ], ['escape' => false]
                                ) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">location_city</i>' . __('Sectors'), [
                                    'plugin' => 'Infrastructure', 'controller' => 'Sectors', 'action' => 'index'
                                ], ['escape' => false]
                                ) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="material-icons left">linear_scale</i>' . __('Status'),
                                    ['plugin' => 'Infrastructure', 'controller' => 'Status', 'action' => 'index'], ['escape' => false]
                                ) ?>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        <?php } ?>
    </li>
</ul>
  