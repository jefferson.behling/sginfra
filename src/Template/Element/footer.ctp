<footer class="page-footer green-background">
    <div class="container">
        <div class="row">
            <p class="col s12 m10 offset-m1">Jefferson Vantuir © <?= date('Y') ?>
                <a class="right white-text text-lighten-3">
                    jefferson.behling@gmail.com
                </a>
            </p>
        </div>
    </div>
</footer>