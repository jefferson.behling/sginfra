<style>
    .tiny-custom:hover {
        cursor: pointer;
    }
</style>
<div class="row">
    <form class="col s12">
      <div id="card-stats">
            <div class="row">
                <div class="col s12 m10 l10 offset-m1 offset-l1">
                    <a href="infrastructure/occurrences/news">
                        <div class="col s12 m6 l3">
                            <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text">
                                <div class="padding-4">
                                    <div class="col s9 m9">
                                        <i class="material-icons background-round mt-5">new_releases</i>
                                        <p><?= __('News Occurrences') ?></p>
                                    </div>
                                    <div class="col s3 m3 right-align">
                                        <h5 class="mb-0"><?= $news ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="infrastructure/occurrences/progress">
                        <div class="col s12 m6 l3">
                            <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text">
                                <div class="padding-4">
                                    <div class="col s9 m9">
                                        <i class="material-icons background-round mt-5">hourglass_full</i>
                                        <p><?= __('In Progress') ?></p>
                                    </div>
                                    <div class="col s3 m3 right-align">
                                        <h5 class="mb-0"><?= $inProgress ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="infrastructure/occurrences/finished">
                        <div class="col s12 m6 l3">
                            <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text">
                                <div class="padding-4">
                                    <div class="col s9 m9">
                                        <i class="material-icons background-round mt-5">check</i>
                                        <p><?= __('Finished') ?></p>
                                    </div>
                                    <div class="col s3 m3 right-align">
                                        <h5 class="mb-0"><?= $finished ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="infrastructure/occurrences/denied">
                        <div class="col s12 m6 l3">
                            <div class="card gradient-45deg-red-light-red gradient-shadow min-height-100 white-text">
                                <div class="padding-4">
                                    <div class="col s9 m9">
                                        <i class="material-icons background-round mt-5">close</i>
                                        <p><?= __('Denied') ?></p>
                                    </div>
                                    <div class="col s3 m3 right-align">
                                        <h5 class="mb-0"><?= $denied ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
          </div>
        </div>
        <div class="row">
            <div class="col s12 m10 l10 offset-m1 offset-l1">
                <div class="card">
                    <div class="card-content">
                        <h5 class="red-text"><?= __("Occurrences") ?></h5>
                        <table class="striped responsive-table">
                          <thead>
                            <tr>
                              <th><?= __('Title') ?></th>
                              <th><?= __('Locale') ?></th>
                              <th><?= __('Status') ?></th>
                              <th><?= __('Sector') ?></th>
                              <th><?= __('Category') ?></th>
                              <th><?= __('Created') ?></th>
                              <th><?= __('Actions') ?></th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php foreach ($occurrences as $occurrence): ?>
                                <tr>
                                  <td><?= $occurrence->title ?></td>
                                  <td><?= $occurrence->locale ?></td>
                                  <td><?= $occurrence->status->name ?></td>
                                  <td><?= $occurrence->sector->name ?></td>
                                  <td><?= $occurrence->category->name ?></td>
                                  <td><?= $occurrence->created->format('d/m/Y H:i:s') ?></td>
                                  <td>
                                      <?= $this->Html->link('<i class="material-icons tiny-custom black-text" title="'. __('View') . '" >zoom_in</i>', ['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'view', $occurrence->id], ['escape' => false]) ?>
                                      <i class="material-icons tiny-custom black-text" title="<?= __('Change Status') ?>" onclick="changeStatus('<?= $occurrence->id ?>')">linear_scale</i>
                                  </td>
                                </tr>
                              <?php endforeach; ?>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal">
    <div class="modal-content">
        <div class="preloader-wrapper small">
            <div class="spinner-layer spinner-green-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <div id="datas"></div>
    </div>
</div>
<script>

    function changeStatus(id) {
        $.ajax({
            url: '<?= $this->Url->build(['plugin' => 'Infrastructure', 'controller' => 'Occurrences', 'action' => 'change-status']) ?>' + '/' + id,
            beforeSend: function() {
                $("#datas").html('');
                $(".modal").modal('open');
                $(".preloader-wrapper").addClass('active');
            }
        })
            .done(function(data) {
                $(".preloader-wrapper").removeClass('active');
                $("#datas").html(data);
            })
    }
</script>
