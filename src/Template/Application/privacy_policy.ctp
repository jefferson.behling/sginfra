<?php
    $this->layout = 'login';
?>
<div class="row">
    <div class="col s12 m12 l10 offset-l1">
        <div class="card darken-1 col s12">
            <div class="card-content black-text">
                <span class="card-title green-text center">
                    <?= __('Politica de Privacidade') ?>
                </span>
            </div>
            <div class="col s12 m10 offset-m1 text-justify">
                <p>
                    Este aplicativo tem o objetivo agilizar o processo de solicitação de manutenção da infraestrutura do Instituto Federal de Educação, Ciência e Tecnologia Farroupilha - Campus São Vicente do Sul.
                </p>
                <p>
                    Este aplicativo requer permissão de acesso à <b>câmera</b> e <b>galeria de fotos</b> para que seja possível capturar imagens dos problemas e/ou carregar imagens da galeria do dispositivo.
                </p>
                <p>Todas as informações fornecidas ao aplicativo são utilizadas apenas dentro do aplicativo, sem segundas intenções.</p>
                <br><br>
                <p><b>Contato:</b> jefferson.behling@gmail.com</p>
            </div>
        </div>
    </div>
</div>
