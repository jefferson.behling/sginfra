<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Occurrence Entity
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $locale
 * @property string $image
 * @property string $observations
 * @property string $user_id
 * @property string $sector_id
 * @property string $status_id
 * @property string $category_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Sector $sector
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\Category $category
 */
class Occurrence extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'description' => true,
        'locale' => true,
        'image' => true,
        'observations' => true,
        'user_id' => true,
        'sector_id' => true,
        'status_id' => true,
        'category_id' => true,
        'user' => true,
        'sector' => true,
        'status' => true,
        'category' => true
    ];
}
