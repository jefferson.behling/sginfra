<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Auth\Storage\StorageInterface;
use Cake\Controller\Controller;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Http\Response;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Accounts/Permissions.CheckPermissions');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'plugin' => null,
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginRedirect' => [
                'plugin' => 'Infrastructure',
                'controller' => 'Occurrences',
                'action' => 'index',
            ],
            'logoutRedirect' => [
                'plugin' => null,
                'controller' => 'Users',
                'action' => 'login',
            ],
            'authError' => false,
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ],
            ],
            'checkAuthIn' => 'Controller.initialize',
        ]);
        $this->loadUserProfile();

        $this->Auth->allow(['login', 'register', 'forgotPassword', 'recoveryPass', 'activateAccount', 'getEmailDomains']);
    }

    private function loadUserProfile()
    {
        $this->set('profile_name', $this->Auth->user('name'));
        $this->set('profile_email', $this->Auth->user('email'));
        $this->set('profile_role', $this->Auth->user('role_id'));
    }
}


