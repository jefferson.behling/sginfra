# SGInfra - Sistema de Gerenciamento da Infraestrutura

Sistema desenvolvido em CakePHP 3 para gerenciar as ocorrências recebidas em relação a infraestrutura do Campus através do Aplicativo Móvel.

## Instalação

Primeiro veja os [requisitos](https://book.cakephp.org/3.0/en/installation.html#requirements) para que o CakePHP funcione corretamente.

Em seu terminal, execute

```bash
git clone https://gitlab.com/jefferson.behling/iffar-infra.git
```
Depois entre na pasta usando

```bash
cd iffar-infra
```
E por fim execute o comando 
```bash
composer update
```
E posteriormente execute
```bash
composer install
```

## Banco de Dados

Para configuração a Base de Dados edite o arquivo ```config/app.php``` na seção de ```Databases```.